-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-08-2016 a las 00:54:18
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contactcenter`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agent_status`
--

CREATE TABLE `agent_status` (
  `agentId` varchar(40) NOT NULL DEFAULT '',
  `agentName` varchar(40) DEFAULT NULL,
  `agentStatus` varchar(30) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `callid` double(18,6) UNSIGNED DEFAULT '0.000000',
  `queue` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `call_status`
--

CREATE TABLE `call_status` (
  `callId` double(18,6) NOT NULL,
  `callerId` varchar(13) NOT NULL,
  `status` varchar(30) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `queue` varchar(25) NOT NULL,
  `position` varchar(11) NOT NULL,
  `originalPosition` varchar(11) NOT NULL,
  `holdtime` varchar(11) NOT NULL,
  `keyPressed` varchar(11) NOT NULL,
  `callduration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_campaign`
--

CREATE TABLE `cc_campaign` (
  `id_campaign` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL COMMENT 'name (human name)',
  `ind_status` bit(1) NOT NULL DEFAULT b'0' COMMENT 'status\nexample:\n0 => inactive\n1 => active\n2 => suspended',
  `id_type_campaign` int(11) NOT NULL,
  `id_client` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_campaign_agent`
--

CREATE TABLE `cc_campaign_agent` (
  `id_campaign` int(11) NOT NULL,
  `id_agent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_campaign_contact`
--

CREATE TABLE `cc_campaign_contact` (
  `id_campaign` int(11) NOT NULL,
  `id_contact` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_client`
--

CREATE TABLE `cc_client` (
  `id_client` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL,
  `des_ruc` varchar(11) DEFAULT NULL,
  `des_address` text,
  `des_phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_contact`
--

CREATE TABLE `cc_contact` (
  `id_contact` int(11) NOT NULL,
  `des_firstname` varchar(60) NOT NULL COMMENT 'firstname contact',
  `des_last_name` varchar(60) DEFAULT NULL COMMENT 'lastname contact'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_contact_detail`
--

CREATE TABLE `cc_contact_detail` (
  `id_contact_detail` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL COMMENT 'name (human name)\nexample:\nEmail\nAddress\nCountry',
  `des_machine_name` varchar(30) NOT NULL COMMENT 'machine_name\nemail\nphone\naddress',
  `des_value` text COMMENT 'value of field',
  `id_contact` int(11) NOT NULL COMMENT 'foreign key of cc_contact'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_agent`
--

CREATE TABLE `gen_agent` (
  `id_agent` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL COMMENT 'name (human name)',
  `des_machine_name` varchar(60) NOT NULL COMMENT 'machine_name (name agente)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_pause`
--

CREATE TABLE `gen_pause` (
  `id_pause` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_perm`
--

CREATE TABLE `gen_perm` (
  `id_perm` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL COMMENT 'name (human name)\nexample:\nReports\nMonitor',
  `des_machine_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gen_perm`
--

INSERT INTO `gen_perm` (`id_perm`, `des_name`, `des_machine_name`) VALUES
(1, 'Permisos', 'perm'),
(2, 'Tipos de Usuario', 'group'),
(3, 'Usuarios', 'user'),
(4, 'Colas', 'queue'),
(5, 'Agentes', 'agent'),
(6, 'Turnos', 'turn'),
(7, 'Campañas', 'campaign'),
(8, 'Asignar agentes a Campañas', 'agent_to_campaign');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_queue`
--

CREATE TABLE `gen_queue` (
  `id_queue` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL COMMENT 'name (human name)',
  `des_machine_name` varchar(40) DEFAULT NULL COMMENT 'machine_name (queue name)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_queue_campaign`
--

CREATE TABLE `gen_queue_campaign` (
  `id_queue` int(11) NOT NULL,
  `id_campaign` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_turn`
--

CREATE TABLE `gen_turn` (
  `id_turn` int(11) NOT NULL,
  `des_name` varchar(30) NOT NULL COMMENT 'name (human name)\nexample:\nmañana\ntarde\nnoche'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_type_campaign`
--

CREATE TABLE `gen_type_campaign` (
  `id_type_campaign` int(11) NOT NULL,
  `des_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_type_user`
--

CREATE TABLE `gen_type_user` (
  `id_type_user` int(11) NOT NULL,
  `des_name` varchar(60) DEFAULT NULL COMMENT 'type user name\nexample:\nAdministrador\nSupervisor\nAgente',
  `des_machine_name` varchar(40) DEFAULT NULL COMMENT 'user type machine name\nexample:\nadmin\nsupervisor\nagent'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gen_type_user`
--

INSERT INTO `gen_type_user` (`id_type_user`, `des_name`, `des_machine_name`) VALUES
(1, 'Administrador', 'admin'),
(2, 'Supervisor', 'supervisor'),
(3, 'Agente', 'agent');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_type_user_perm`
--

CREATE TABLE `gen_type_user_perm` (
  `id_type_user` int(11) NOT NULL COMMENT 'foreign key for gen_type_perm',
  `id_perm` int(11) NOT NULL COMMENT 'foreign key of gen_perm',
  `ind_add` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled',
  `ind_edit` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled',
  `ind_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled',
  `ind_show` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_user`
--

CREATE TABLE `gen_user` (
  `id_user` int(11) NOT NULL,
  `des_user` varchar(80) DEFAULT NULL COMMENT 'user login',
  `des_firstname` varchar(60) DEFAULT NULL COMMENT 'user first name',
  `des_lastname` varchar(60) DEFAULT NULL COMMENT 'user last name',
  `des_email` varchar(80) DEFAULT NULL COMMENT 'user email',
  `des_passwd` text COMMENT 'user password',
  `des_salt` text COMMENT 'user salt encrypt password',
  `ind_status` bit(1) DEFAULT b'0' COMMENT 'user status\n0 => inactive\n1 => active\n2 => suspended',
  `id_type_user` int(11) DEFAULT NULL COMMENT 'user type\ntable gen_type_user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gen_user`
--

INSERT INTO `gen_user` (`id_user`, `des_user`, `des_firstname`, `des_lastname`, `des_email`, `des_passwd`, `des_salt`, `ind_status`, `id_type_user`) VALUES
(1, 'lgonzales', 'Jose Luis', 'Huamani Gonzales', 'Aisenhaim@gmail.com', '123456', '12345', b'1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_user_agent`
--

CREATE TABLE `gen_user_agent` (
  `id_user` int(11) NOT NULL,
  `id_agent` int(11) NOT NULL,
  `id_turn` int(11) NOT NULL COMMENT 'turn id foreign'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_user_perm`
--

CREATE TABLE `gen_user_perm` (
  `id_user` int(11) NOT NULL COMMENT 'foreign key of gen_user',
  `id_perm` int(11) NOT NULL COMMENT 'foreign key of gen_perm',
  `ind_add` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled',
  `ind_edit` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled',
  `ind_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled',
  `ind_show` bit(1) NOT NULL DEFAULT b'0' COMMENT 'values:\n0 => disabled\n1 => enabled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `queue_log`
--

CREATE TABLE `queue_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `time` varchar(40) DEFAULT NULL,
  `callid` varchar(32) NOT NULL DEFAULT '',
  `queuename` varchar(32) NOT NULL DEFAULT '',
  `agent` varchar(32) NOT NULL DEFAULT '',
  `event` varchar(32) NOT NULL DEFAULT '',
  `data` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `queue_log`
--
DELIMITER $$
CREATE TRIGGER `queueEvents` BEFORE INSERT ON `queue_log` FOR EACH ROW BEGIN
	IF NEW.event = 'ADDMEMBER' THEN
		INSERT INTO agent_status (agentId,agentStatus,timestamp,callid) VALUES (NEW.agent,'READY',FROM_UNIXTIME(NEW.time),NULL) ON DUPLICATE KEY UPDATE agentStatus = "READY", timestamp = FROM_UNIXTIME(NEW.time), callid = NULL;
	ELSEIF NEW.event = 'REMOVEMEMBER' THEN
		INSERT INTO `bit_agent_status` (agentId,agentStatus,timestamp,callid) VALUES (NEW.agent,'LOGGEDOUT',FROM_UNIXTIME(NEW.time),NULL) ON DUPLICATE KEY UPDATE agentStatus = "LOGGEDOUT", timestamp = FROM_UNIXTIME(NEW.time), callid = NULL;
	ELSEIF NEW.event = 'PAUSE' THEN
		INSERT INTO agent_status (agentId,agentStatus,timestamp,callid) VALUES (NEW.agent,'PAUSE',FROM_UNIXTIME(NEW.time),NULL) ON DUPLICATE KEY UPDATE agentStatus = "PAUSE", timestamp = FROM_UNIXTIME(NEW.time), callid = NULL;
	ELSEIF NEW.event = 'UNPAUSE' THEN
		INSERT INTO `bit_agent_status` (agentId,agentStatus,timestamp,callid) VALUES (NEW.agent,'READY',FROM_UNIXTIME(NEW.time),NULL) ON DUPLICATE KEY UPDATE agentStatus = "READY", timestamp = FROM_UNIXTIME(NEW.time), callid = NULL;
	ELSEIF NEW.event = 'ENTERQUEUE' THEN
		REPLACE INTO `call_status` VALUES
		(NEW.callid,
		replace(replace(substring(substring_index(NEW.data, '|', 2), length(substring_index(New.data, '|', 2 - 1)) + 1), '|', ''), '|', ''),
		'inQue',
		FROM_UNIXTIME(NEW.time),
		NEW.queuename,
		'',
		'',
		'',
		'',
		0);
	ELSEIF NEW.event = 'CONNECT' THEN
		UPDATE `call_status` SET
			callid = NEW.callid,
			status = NEW.event,
			timestamp = FROM_UNIXTIME(NEW.time),
			queue = NEW.queuename,
			holdtime = replace(substring(substring_index(NEW.data, '|', 1), length(substring_index(NEW.data, '|', 1 - 1)) + 1), '|', '')
		where callid = NEW.callid;
		INSERT INTO agent_status (agentId,agentStatus,timestamp,callid) VALUES
		(NEW.agent,NEW.event,
		FROM_UNIXTIME(NEW.time),
		NEW.callid)
		ON DUPLICATE KEY UPDATE
			agentStatus = NEW.event,
			timestamp = FROM_UNIXTIME(NEW.time),
			callid = NEW.callid;
	ELSEIF NEW.event in ('COMPLETECALLER','COMPLETEAGENT') THEN
		UPDATE `call_status` SET
			callid = NEW.callid,
			status = NEW.event,
			timestamp = FROM_UNIXTIME(NEW.time),
			queue = NEW.queuename,
			originalPosition = replace(substring(substring_index(NEW.data, '|', 3), length(substring_index(NEW.data, '|', 3 - 1)) + 1), '|', ''),
			holdtime = replace(substring(substring_index(NEW.data, '|', 1), length(substring_index(NEW.data, '|', 1 - 1)) + 1), '|', ''),
			callduration = replace(substring(substring_index(NEW.data, '|', 2), length(substring_index(NEW.data, '|', 2 - 1)) + 1), '|', '')
		where callid = NEW.callid;
		INSERT INTO agent_status (agentId,agentStatus,timestamp,callid) VALUES (NEW.agent,NEW.event,FROM_UNIXTIME(NEW.time),NULL) ON DUPLICATE KEY UPDATE agentStatus = "READY", timestamp = FROM_UNIXTIME(NEW.time), callid = NULL;
	ELSEIF NEW.event in ('TRANSFER') THEN
UPDATE `call_status` SET
callid = NEW.callid,
status = NEW.event,
timestamp = FROM_UNIXTIME(NEW.time),
queue = NEW.queuename,
holdtime = replace(substring(substring_index(NEW.data, '|', 1), length(substring_index(NEW.data, '|', 1 - 1)) + 1), '|', ''),
callduration = replace(substring(substring_index(NEW.data, '|', 3), length(substring_index(NEW.data, '|', 3 - 1)) + 1), '|', '')
where callid = NEW.callid;
INSERT INTO agent_status (agentId,agentStatus,timestamp,callid) VALUES
(NEW.agent,'READY',FROM_UNIXTIME(NEW.time),NULL)
ON DUPLICATE KEY UPDATE
agentStatus = "READY",
timestamp = FROM_UNIXTIME(NEW.time),
callid = NULL;
	ELSEIF NEW.event in ('ABANDON','EXITEMPTY') THEN
UPDATE `call_status` SET
callid = NEW.callid,
status = NEW.event,
timestamp = FROM_UNIXTIME(NEW.time),
queue = NEW.queuename,
position = replace(substring(substring_index(NEW.data, '|', 1), length(substring_index(NEW.data, '|', 1 - 1)) + 1), '|', ''),
originalPosition = replace(substring(substring_index(NEW.data, '|', 2), length(substring_index(NEW.data, '|', 2 - 1)) + 1), '|', ''),
holdtime = replace(substring(substring_index(NEW.data, '|', 3), length(substring_index(NEW.data, '|', 3 - 1)) + 1), '|', '')
where callid = NEW.callid;
	ELSEIF NEW.event = 'EXITWITHKEY'THEN
		UPDATE `call_status` SET
			callid = NEW.callid,
			status = NEW.event,
			timestamp = FROM_UNIXTIME(NEW.time),
			queue = NEW.queuename,
			position = replace(substring(substring_index(NEW.data, '|', 2), length(substring_index(NEW.data, '|', 2 - 1)) + 1), '|', ''),
			keyPressed = replace(substring(substring_index(NEW.data, '|', 1), length(substring_index(NEW.data, '|', 1 - 1)) + 1), '|', '')
		where callid = NEW.callid;
	ELSEIF NEW.event = 'EXITWITHTIMEOUT' THEN
		UPDATE `call_status` SET
			callid = NEW.callid,
			status = NEW.event,
			timestamp = FROM_UNIXTIME(NEW.time),
			queue = NEW.queuename,
			position = replace(substring(substring_index(NEW.data, '|', 1), length(substring_index(NEW.data, '|', 1 - 1)) + 1), '|', '')
		where callid = NEW.callid;
END IF;
END
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agent_status`
--
ALTER TABLE `agent_status`
  ADD PRIMARY KEY (`agentId`),
  ADD KEY `agentName` (`agentName`),
  ADD KEY `agentStatus` (`agentStatus`,`timestamp`,`callid`),
  ADD KEY `queue` (`queue`);

--
-- Indices de la tabla `call_status`
--
ALTER TABLE `call_status`
  ADD PRIMARY KEY (`callId`),
  ADD KEY `callerId` (`callerId`),
  ADD KEY `status` (`status`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `queue` (`queue`),
  ADD KEY `position` (`position`,`originalPosition`,`holdtime`);

--
-- Indices de la tabla `cc_campaign`
--
ALTER TABLE `cc_campaign`
  ADD PRIMARY KEY (`id_campaign`,`id_type_campaign`,`id_client`),
  ADD UNIQUE KEY `id_campaign_UNIQUE` (`id_campaign`),
  ADD KEY `fk_cc_campaign_gen_type_campaign1_idx` (`id_type_campaign`),
  ADD KEY `fk_cc_campaign_cc_client1_idx` (`id_client`);

--
-- Indices de la tabla `cc_campaign_agent`
--
ALTER TABLE `cc_campaign_agent`
  ADD PRIMARY KEY (`id_campaign`,`id_agent`),
  ADD KEY `fk_cc_campaign_has_gen_agent_gen_agent1_idx` (`id_agent`),
  ADD KEY `fk_cc_campaign_has_gen_agent_cc_campaign1_idx` (`id_campaign`);

--
-- Indices de la tabla `cc_campaign_contact`
--
ALTER TABLE `cc_campaign_contact`
  ADD PRIMARY KEY (`id_campaign`,`id_contact`),
  ADD KEY `fk_cc_campaign_has_cc_contact_cc_contact1_idx` (`id_contact`),
  ADD KEY `fk_cc_campaign_has_cc_contact_cc_campaign1_idx` (`id_campaign`);

--
-- Indices de la tabla `cc_client`
--
ALTER TABLE `cc_client`
  ADD PRIMARY KEY (`id_client`),
  ADD UNIQUE KEY `id_client_UNIQUE` (`id_client`);

--
-- Indices de la tabla `cc_contact`
--
ALTER TABLE `cc_contact`
  ADD PRIMARY KEY (`id_contact`),
  ADD UNIQUE KEY `id_contact_UNIQUE` (`id_contact`);

--
-- Indices de la tabla `cc_contact_detail`
--
ALTER TABLE `cc_contact_detail`
  ADD PRIMARY KEY (`id_contact_detail`,`id_contact`),
  ADD UNIQUE KEY `id_contact_detail_UNIQUE` (`id_contact_detail`),
  ADD KEY `fk_cc_contact_detail_cc_contact1_idx` (`id_contact`);

--
-- Indices de la tabla `gen_agent`
--
ALTER TABLE `gen_agent`
  ADD PRIMARY KEY (`id_agent`),
  ADD UNIQUE KEY `id_agent_UNIQUE` (`id_agent`),
  ADD UNIQUE KEY `des_machine_name_UNIQUE` (`des_machine_name`);

--
-- Indices de la tabla `gen_pause`
--
ALTER TABLE `gen_pause`
  ADD PRIMARY KEY (`id_pause`),
  ADD UNIQUE KEY `id_pause_UNIQUE` (`id_pause`);

--
-- Indices de la tabla `gen_perm`
--
ALTER TABLE `gen_perm`
  ADD PRIMARY KEY (`id_perm`),
  ADD UNIQUE KEY `id_perm_UNIQUE` (`id_perm`),
  ADD UNIQUE KEY `des_machine_name_UNIQUE` (`des_machine_name`),
  ADD UNIQUE KEY `des_name_UNIQUE` (`des_name`);

--
-- Indices de la tabla `gen_queue`
--
ALTER TABLE `gen_queue`
  ADD PRIMARY KEY (`id_queue`),
  ADD UNIQUE KEY `id_queue_UNIQUE` (`id_queue`),
  ADD UNIQUE KEY `des_machine_name_UNIQUE` (`des_machine_name`);

--
-- Indices de la tabla `gen_queue_campaign`
--
ALTER TABLE `gen_queue_campaign`
  ADD PRIMARY KEY (`id_queue`,`id_campaign`),
  ADD KEY `fk_gen_queue_has_cc_campaign_cc_campaign1_idx` (`id_campaign`),
  ADD KEY `fk_gen_queue_has_cc_campaign_gen_queue1_idx` (`id_queue`);

--
-- Indices de la tabla `gen_turn`
--
ALTER TABLE `gen_turn`
  ADD PRIMARY KEY (`id_turn`),
  ADD UNIQUE KEY `id_turn_UNIQUE` (`id_turn`),
  ADD UNIQUE KEY `des_name_UNIQUE` (`des_name`);

--
-- Indices de la tabla `gen_type_campaign`
--
ALTER TABLE `gen_type_campaign`
  ADD PRIMARY KEY (`id_type_campaign`),
  ADD UNIQUE KEY `id_type_campaign_UNIQUE` (`id_type_campaign`);

--
-- Indices de la tabla `gen_type_user`
--
ALTER TABLE `gen_type_user`
  ADD PRIMARY KEY (`id_type_user`),
  ADD UNIQUE KEY `id_type_user_UNIQUE` (`id_type_user`),
  ADD UNIQUE KEY `des_machine_name_UNIQUE` (`des_machine_name`);

--
-- Indices de la tabla `gen_type_user_perm`
--
ALTER TABLE `gen_type_user_perm`
  ADD PRIMARY KEY (`id_type_user`,`id_perm`),
  ADD KEY `fk_gen_type_user_has_gen_perm_gen_perm1_idx` (`id_perm`),
  ADD KEY `fk_gen_type_user_has_gen_perm_gen_type_user1_idx` (`id_type_user`);

--
-- Indices de la tabla `gen_user`
--
ALTER TABLE `gen_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `id_user_UNIQUE` (`id_user`),
  ADD UNIQUE KEY `des_user_UNIQUE` (`des_user`),
  ADD KEY `fk_gen_user_gen_type_user_idx` (`id_type_user`);

--
-- Indices de la tabla `gen_user_agent`
--
ALTER TABLE `gen_user_agent`
  ADD PRIMARY KEY (`id_user`,`id_agent`,`id_turn`),
  ADD KEY `fk_gen_user_has_gen_agent_gen_agent1_idx` (`id_agent`),
  ADD KEY `fk_gen_user_has_gen_agent_gen_user1_idx` (`id_user`),
  ADD KEY `fk_gen_user_has_gen_agent_gen_turn1_idx` (`id_turn`);

--
-- Indices de la tabla `gen_user_perm`
--
ALTER TABLE `gen_user_perm`
  ADD PRIMARY KEY (`id_user`,`id_perm`),
  ADD KEY `fk_gen_user_has_gen_perm_gen_perm1_idx` (`id_perm`),
  ADD KEY `fk_gen_user_has_gen_perm_gen_user1_idx` (`id_user`);

--
-- Indices de la tabla `queue_log`
--
ALTER TABLE `queue_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cc_campaign`
--
ALTER TABLE `cc_campaign`
  MODIFY `id_campaign` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cc_client`
--
ALTER TABLE `cc_client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cc_contact`
--
ALTER TABLE `cc_contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cc_contact_detail`
--
ALTER TABLE `cc_contact_detail`
  MODIFY `id_contact_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gen_agent`
--
ALTER TABLE `gen_agent`
  MODIFY `id_agent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gen_pause`
--
ALTER TABLE `gen_pause`
  MODIFY `id_pause` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gen_perm`
--
ALTER TABLE `gen_perm`
  MODIFY `id_perm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `gen_queue`
--
ALTER TABLE `gen_queue`
  MODIFY `id_queue` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gen_turn`
--
ALTER TABLE `gen_turn`
  MODIFY `id_turn` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gen_type_campaign`
--
ALTER TABLE `gen_type_campaign`
  MODIFY `id_type_campaign` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gen_type_user`
--
ALTER TABLE `gen_type_user`
  MODIFY `id_type_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `gen_user`
--
ALTER TABLE `gen_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `queue_log`
--
ALTER TABLE `queue_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cc_campaign`
--
ALTER TABLE `cc_campaign`
  ADD CONSTRAINT `fk_cc_campaign_cc_client1` FOREIGN KEY (`id_client`) REFERENCES `cc_client` (`id_client`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cc_campaign_gen_type_campaign1` FOREIGN KEY (`id_type_campaign`) REFERENCES `gen_type_campaign` (`id_type_campaign`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cc_campaign_agent`
--
ALTER TABLE `cc_campaign_agent`
  ADD CONSTRAINT `fk_cc_campaign_has_gen_agent_cc_campaign1` FOREIGN KEY (`id_campaign`) REFERENCES `cc_campaign` (`id_campaign`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cc_campaign_has_gen_agent_gen_agent1` FOREIGN KEY (`id_agent`) REFERENCES `gen_agent` (`id_agent`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cc_campaign_contact`
--
ALTER TABLE `cc_campaign_contact`
  ADD CONSTRAINT `fk_cc_campaign_has_cc_contact_cc_campaign1` FOREIGN KEY (`id_campaign`) REFERENCES `cc_campaign` (`id_campaign`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cc_campaign_has_cc_contact_cc_contact1` FOREIGN KEY (`id_contact`) REFERENCES `cc_contact` (`id_contact`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cc_contact_detail`
--
ALTER TABLE `cc_contact_detail`
  ADD CONSTRAINT `fk_cc_contact_detail_cc_contact1` FOREIGN KEY (`id_contact`) REFERENCES `cc_contact` (`id_contact`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gen_queue_campaign`
--
ALTER TABLE `gen_queue_campaign`
  ADD CONSTRAINT `fk_gen_queue_has_cc_campaign_cc_campaign1` FOREIGN KEY (`id_campaign`) REFERENCES `cc_campaign` (`id_campaign`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gen_queue_has_cc_campaign_gen_queue1` FOREIGN KEY (`id_queue`) REFERENCES `gen_queue` (`id_queue`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gen_type_user_perm`
--
ALTER TABLE `gen_type_user_perm`
  ADD CONSTRAINT `fk_gen_type_user_has_gen_perm_gen_perm1` FOREIGN KEY (`id_perm`) REFERENCES `gen_perm` (`id_perm`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gen_type_user_has_gen_perm_gen_type_user1` FOREIGN KEY (`id_type_user`) REFERENCES `gen_type_user` (`id_type_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gen_user`
--
ALTER TABLE `gen_user`
  ADD CONSTRAINT `fk_gen_user_gen_type_user` FOREIGN KEY (`id_type_user`) REFERENCES `gen_type_user` (`id_type_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gen_user_agent`
--
ALTER TABLE `gen_user_agent`
  ADD CONSTRAINT `fk_gen_user_has_gen_agent_gen_agent1` FOREIGN KEY (`id_agent`) REFERENCES `gen_agent` (`id_agent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gen_user_has_gen_agent_gen_turn1` FOREIGN KEY (`id_turn`) REFERENCES `gen_turn` (`id_turn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gen_user_has_gen_agent_gen_user1` FOREIGN KEY (`id_user`) REFERENCES `gen_user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gen_user_perm`
--
ALTER TABLE `gen_user_perm`
  ADD CONSTRAINT `fk_gen_user_has_gen_perm_gen_perm1` FOREIGN KEY (`id_perm`) REFERENCES `gen_perm` (`id_perm`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gen_user_has_gen_perm_gen_user1` FOREIGN KEY (`id_user`) REFERENCES `gen_user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
