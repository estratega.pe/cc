<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('campaign/campaignModel', 'campaign');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_campaign']
				,['data' => 'des_name']
				,['data' => 'ind_status']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'campaign/viewCampaign/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'campaign/editCampaign/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'campaign/deleteCampaign/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Campa&ntilde;as', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Campa&ntilde;as'
			,'URL_AJAX'			=> base_url() . 'campaign/getCampaigns/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Campa&ntilde;as'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_campaign'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'campaign/addCampaign', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'ACTIVO'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addCampaign() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$values = ['des_name' => $this->input->post('input_des_name', TRUE)
					,'ind_status'		=> 0
					,'id_type_campaign'	=> 1
					,'id_client'		=> $this->input->post('select_id_client')
			];
			$newID = $this->campaign->saveCampaign($values, $this->input->post('select_id_queue', TRUE));
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'campaign/editCampaign/' . $newID);
			}
		}
		$p_select_queues = [];
		$queues = $this->db->get_where('gen_queue')->result();
		foreach($queues as $k) {
			$p_select_queues[$k->id_queue] = $k->des_name;
		}
		$p_select_clients = [];
		$clients = $this->db->get_where('cc_client')->result();
		foreach($clients as $c) {
			$p_select_clients[$c->id_client] = $c->des_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nueva Campa&ntilde;a', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('campaign/addCampaign', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nueva Campa&ntilde;a'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'campaign/addCampaign/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'SELECT_ID_QUEUE'			=> form_dropdown('select_id_queue', $p_select_queues, '', 'class="form-control" required')
			,'SELECT_ID_CLIENT'			=> form_dropdown('select_id_client', $p_select_clients, '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'campaign', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editCampaign() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->campaign->getCampaignRow(['id_campaign' => $ID]);
		if( $this->input->post() ) {
			$values = ['des_name'	=> $this->input->post('input_des_name', TRUE)
					,'ind_status'	=> intval($this->input->post('checkbox_ind_status'))
					,'id_client'	=> $this->input->post('select_id_client')
			];
			if( $this->campaign->updateCampaign($values, $this->input->post('select_id_queue'), $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				regenerateQueues();
				redirect(base_url() . 'campaign/viewCampaign/' . $ID);
			}
		}
		$p_select_ind_activo = [];
		$opciones_sino = $this->config->item('OPTIONS_SINO');
		while ($name = current($opciones_sino)) {
			array_push($p_select_ind_activo, ['ID' => key($opciones_sino), 'NAME' => $name]);
			next($opciones_sino);
		}
		$p_select_agents = [];
		$ag = modules::run('agent/getAgents', 'object');
		foreach($ag as $k) {
			$p_select_agents[$k->id_agent] = $k->des_name;
		}
		$p_selected_agents = [];
		$sa = $this->campaign->getCampaignAgents(['id_campaign' => $ID]);
		foreach($sa as $k) {
			$p_selected_agents[] = $k->id_agent;
		}
		$p_select_queues = [];
		$queues = $this->db->get_where('gen_queue')->result();
		foreach($queues as $k) {
			$p_select_queues[$k->id_queue] = $k->des_name;
		}
		$p_select_clients = [];
		$clients = $this->db->get_where('cc_client')->result();
		foreach($clients as $c) {
			$p_select_clients[$c->id_client] = $c->des_name;
		}
		$queue_selected = $this->db->get_where('gen_queue_campaign', ['id_campaign' => $ID])->row();
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Campa&ntilde;a', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('campaign/editCampaign', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Campa&ntilde;a'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() . 'campaign/editCampaign/' . $p->id_campaign
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'CHECKBOX_IND_STATUS'		=> form_checkbox('checkbox_ind_status', 1, ($p->ind_status==0x0001) ? TRUE: FALSE, ' id="checkbox_ind_status" class="input-switch-alt"')
			,'SELECT_AGENTS'			=> form_dropdown('select_agents', $p_select_agents, $p_selected_agents, ' id="select_agents" multiple="multiple"')
			,'SELECT_ID_QUEUE'			=> form_dropdown('select_id_queue', $p_select_queues, $queue_selected->id_queue, 'class="form-control" required')
			,'SELECT_ID_CLIENT'			=> form_dropdown('select_id_client', $p_select_clients, $p->id_campaign, 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'campaign', 'btn-danger', 'icon-ban', 'Cancelar', true)
				,'URL_SET_CAMPAIGN_AGENT'	=> base_url() . 'campaign/setCampaignAgent/json/' . $p->id_campaign
				,'URL_DEL_CAMPAIGN_AGENT'	=> base_url() . 'campaign/deleteCampaignAgent/json/' . $p->id_campaign
		], TRUE);
		renderPage($data);
	}
	
	public function viewGroup() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'group');
			exit();
		}
		$p = $this->group->getGroupRow(['id_type_user' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Tipo de Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('group/viewGroup', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Tipo de Usuario'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'group', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteCampaign() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'campaign');
			exit();
		}
		$p = $this->campaign->getCampaignRow(['id_campaign' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_campaign') == $ID) {
				if ($this->campaign->deleteCampaign($this->input->post('input_id_campaign')) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'campaign');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Campa&ntilde;a ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Campa&ntilde;a - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->id_campaign . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion de la Campa&ntilde;a "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'campaign/deleteCampaign/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_campaign', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'campaign', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getCampaigns() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->campaign->getCampaigns(null, $like);
				$total = $this->campaign->getTotalCampaign(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->campaign->getCampaigns(null, $like, $start, $limit);
					$total = $this->campaign->getTotalCampaign(null, $like);
				} else {
					$result = $this->campaign->getCampaigns(null, null, $start, $limit);
					$total = $this->campaign->getTotalCampaign();
				}
			}
		} else {
			$result = $this->campaign->getCampaigns();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_campaign'	=> $r->id_campaign
							,'des_name'		=> $r->des_name
							,'ind_status'	=> $this->config->item('OPTIONS_SINO')[$r->ind_status]
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				
			break;
		}
	}
	
	#Agents To Campaign
	public function setCampaignAgent() {
		if( !acl('ind_add', 'agent_to_campaign') ) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No tiene permisos suficientes.']));
		} else {
			$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
			$ID = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(4);
			$id_agent = $this->input->post('agent');
			$values = ['id_campaign' => $ID, 'id_agent' => $id_agent[0]];
			switch($format) {
				case 'json':
					if($this->campaign->setCampaignAgent($values) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode(['isError' => true, 'msg' => 'Error al guardar intentelo mas tarde.']));
					} else {
						regenerateQueues();
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode(['isError' => true, 'title' => 'Correcto', 'msg' => 'Agente agregado correctamente.']));
					}
					break;
			}
		}
	}
	
	public function deleteCampaignAgent() {
		if( !acl('ind_del', 'agent_to_campaign') ) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No tiene permisos suficientes.']));
		} else {
			$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
			$ID = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(4);
			$id_agent = $this->input->post('agent');
			$values = ['id_campaign' => $ID, 'id_agent' => $id_agent[0]];
			switch($format) {
				case 'json':
					if($this->campaign->deleteCampaignAgent($values) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode(['isError' => true, 'msg' => 'Error al eliminar, intentelo mas tarde.']));
					} else {
						regenerateQueues();
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode(['isError' => true, 'title' => 'Correcto', 'msg' => 'Agente eliminado correctamente.']));
					}
					break;
			}
		}
	}
}
