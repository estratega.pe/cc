<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/parsley/parsley.js"></script>

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/lou-multi-select/css/multi-select.css">

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/icon.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/easyui/extensions/jquery-easyui-edatagrid/jquery.edatagrid.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/lou-multi-select/js/jquery.multi-select.js"></script>
<script>
$(function(){
	$('#select_agents').multiSelect({
		selectableHeader: "<div class='content-box-header bg-blue-alt pad5T pad15L pad5B'>Disponibles</div>"
		,selectionHeader: "<div class='content-box-header bg-blue pad5T pad15L pad5B'>Seleccionados</div>"
		,afterSelect: function(agent){
			$.post( "{URL_SET_CAMPAIGN_AGENT}", {agent: agent }, function(row) {
				if(row.title)
					title = row.title;
				else 
					title = 'Error';
				$.messager.show({
					title: title,
					msg:row.msg,
					timeout:2000,
					showType:'slide'
				});
			});
		}
	  	,afterDeselect: function(agent){
	  		$.post( "{URL_DEL_CAMPAIGN_AGENT}", {agent: agent }, function(row) {
				if(row.title)
					title = row.title;
				else 
					title = 'Error';
				$.messager.show({
					title: title,
					msg:row.msg,
					timeout:2000,
					showType:'slide'
				});
			});
	    }
	});
});
</script>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>


<div class="panel-B">
    <div class="panel-B-body">
		<div class="row mailbox-wrapper">
			<div class="col-md-4">
				<div class="content-box pad15B">
					<form class="form-horizontal pad15L pad15R bordered-row" method="post" action="{URL_POST}" enctype="multipart/form-data" data-parsley-validate>
						<h3 class="content-box-header clearfix">CAMPA&Ntilde;A</h3>
						<div class="form-group remove-border">
							<label class="col-sm-3 control-label">CAMPA&Ntilde;A:</label>
							<div class="col-sm-9">
								{INPUT_DES_NAME}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">COLA:</label>
							<div class="col-sm-6">
								{SELECT_ID_QUEUE}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">CLIENTE:</label>
							<div class="col-sm-6">
								{SELECT_ID_CLIENT}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">ACTIVO:</label>
							<div class="col-sm-6">
								{CHECKBOX_IND_STATUS}
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-5 float-left">
								{BUTTON_SUBMIT}
							</div>
							<div class="col-sm-5 float-right">
								{BUTTON_CANCEL}
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="col-md-8">
				<div class="content-box pad15B clearfix">
						<h3 class="content-box-header ">AGENTES</h3>
						<div class="form-group remove-border">
							<label class="col-sm-3 control-label clearfix">AGENTES:</label>
							<div class="col-sm-9">
								{SELECT_AGENTS}
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>

