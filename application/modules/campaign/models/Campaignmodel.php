<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class campaignModel extends MX_Controller {
	
	const CAMPAIGN = 'cc_campaign';
	const AGENT_TO_CAMPAIGN = 'cc_campaign_agent';
	const QUEUE_TO_CAMPAIGN = "gen_queue_campaign";
	const CAMPAIGN_TO_CONTACT = "cc_campaign_contact";
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getCampaigns($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::CAMPAIGN)->result();
    }
    
    function getCampaignRow($where) {
    	return $this->db->get_where(self::CAMPAIGN, $where)->row();
    }
	
	function saveCampaign($data, $queue) {
		$this->db->trans_start();
		$this->db->insert(self::CAMPAIGN, $data);
		$ID = $this->db->insert_id();
		$this->db->insert(self::QUEUE_TO_CAMPAIGN, ['id_campaign' => $ID, 'id_queue' => $queue]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return $ID;
	}
	
	function updateCampaign($data, $queue, $ID) {
		$this->db->trans_start();
		$this->db->update(self::QUEUE_TO_CAMPAIGN, ['id_campaign' => $ID, 'id_queue' => $queue]);
		$this->db->update(self::CAMPAIGN, $data, ['id_campaign' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteCampaign($ID) {
		$this->db->trans_start();
		$this->db->delete(self::QUEUE_TO_CAMPAIGN, ['id_campaign' => $ID]);
		$this->db->delete(self::AGENT_TO_CAMPAIGN, ['id_campaign' => $ID]);
		$this->db->delete(self::CAMPAIGN_TO_CONTACT, ['id_campaign' => $ID]);
		$this->db->delete(self::CAMPAIGN, ['id_campaign' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalCampaign($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::CAMPAIGN);
		return $this->db->count_all_results();
	}
	
	#AgentToCampaign
	function getCampaignAgents($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		return $this->db->get(self::AGENT_TO_CAMPAIGN)->result();
	}
	function setCampaignAgent($data) {
		$this->db->trans_start();
		$this->db->insert(self::AGENT_TO_CAMPAIGN, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	function deleteCampaignAgent($data) {
		$this->db->trans_start();
		$this->db->delete(self::AGENT_TO_CAMPAIGN, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
}
