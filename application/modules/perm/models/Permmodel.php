<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class permModel extends MX_Controller {
	
	const PERMS = 'gen_perm';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getPerms($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::PERMS)->result();
    }
    
    function getPermRow($where) {
    	return $this->db->get_where(self::PERMS, $where)->row();
    }
	
	function savePerm($data) {
		$this->db->insert(self::PERMS, $data);
		return $this->db->insert_id();
	}
	
	function updatePerm($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::PERMS, $data, ['id_perm' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deletePerm($ID) {
		$this->db->trans_start();
		$this->db->delete(self::PERMS, ['id_perm' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalPerm($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::PERMS);
		return $this->db->count_all_results();
	}
}
