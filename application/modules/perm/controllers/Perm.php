<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perm extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('perm/permModel', 'perm');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_perm']
				,['data' => 'des_name']
				,['data' => 'des_machine_name']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'perm/viewPerm/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'perm/editPerm/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'perm/deletePerm/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Permisos', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Permisos'
			,'URL_AJAX'			=> base_url() . 'perm/getPerms/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Permisos'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_perm'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'perm/addPerm', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'KEY'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addPerm() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->perm->savePerm(['des_name' => $this->input->post('input_des_name', TRUE), 'des_machine_name' => $this->input->post('input_des_machine_name', TRUE)]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'perm/viewPerm/' . $newID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Permiso', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('perm/addPerm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Permiso'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'perm/addPerm/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'perm', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editPerm() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->perm->getPermRow(['id_perm' => $ID]);
		if( $this->input->post() ) {
			if( $this->perm->updatePerm(['des_name'=> $this->input->post('input_des_name', TRUE)], $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'perm/viewPerm/' . $ID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Permiso', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('perm/editPerm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Permiso'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'perm/editPerm/' . $p->id_perm
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_ID_PERM'			=> form_hidden('input_id_perm', $p->id_perm)
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'perm', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewPerm() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'perm');
			exit();
		}
		$p = $this->perm->getPermRow(['id_perm' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Permiso', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('perm/viewPerm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Permiso'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'perm', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deletePerm() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'perm');
			exit();
		}
		$p = $this->perm->getPermRow(['id_perm' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_perm', TRUE) == $ID) {
				if ($this->perm->deletePerm($this->input->post('input_id_perm', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'perm');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Permiso ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Permiso - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->des_machine_name . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del permiso "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'perm/deletePerm/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_perm', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'perm', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getPerms() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->perm->getPerms(null, $like);
				$total = $this->perm->getTotalPerm(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->perm->getPerms(null, $like, $start, $limit);
					$total = $this->perm->getTotalPerm(null, $like);
				} else {
					$result = $this->perm->getPerms(null, null, $start, $limit);
					$total = $this->perm->getTotalPerm();
				}
			}
		} else {
			$result = $this->perm->getPerms();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_perm'			=> $r->id_perm
							,'des_name'			=> $r->des_name
							,'des_machine_name'	=> $r->des_machine_name
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_perm
							,'name'		=> $k->des_name
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
			break;
			default:
				
			break;
		}
	}
}
