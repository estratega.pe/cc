<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('user/userModel', 'user');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_user']
				,['data' => 'des_user']
				,['data' => 'des_firstname']
				,['data' => 'des_lastname']
				,['data' => 'des_email']
				,['data' => 'ind_status']
				,['data' => 'id_type_user']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'user/viewUser/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'user/editUser/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'user/deleteUser/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Usuarios', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Usuarios'
			,'URL_AJAX'			=> base_url() . 'user/getUsers/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Usuarios'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_user'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'user/addUser', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'USUARIO'], ['LABEL' => 'NOMBRES'], ['LABEL' => 'APELLIDOS'], ['LABEL' => 'EMAIL'], ['LABEL' => 'ACTIVO'], ['LABEL' => 'TIPO'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addUser() {
		acl('ind_add', null, 403);
		$typeUsers = $this->user->getTypeUsers();
		if($this->input->post()) {
			$newID = $this->user->saveUser([
					'des_user'			=> $this->input->post('input_des_user', TRUE)
					,'des_firstname'	=> $this->input->post('input_des_firstname', TRUE)
					,'des_lastname'		=> $this->input->post('input_des_lastname', TRUE)
					,'des_email'		=> $this->input->post('input_des_email', TRUE)
					,'des_passwd'		=> $this->input->post('input_des_passwd', TRUE)
					,'ind_status'		=> intval($this->input->post('select_ind_activo'))
					,'id_type_user'		=> $this->input->post('select_id_type_user')
			]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'user/editUser/' . $newID);
			}
		}
		$userTypes = [];
		foreach($typeUsers as $t) {
			$userTypes[$t->id_type_user] = $t->des_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('user/addUser', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Nuevo Usuario'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() .'user/addUser'
				,'INPUT_DES_USER'			=> form_input('input_des_user', '', 'class="form-control" required')
				,'INPUT_DES_FIRSTNAME'		=> form_input('input_des_firstname', '', 'class="form-control" required')
				,'INPUT_DES_LASTNAME'		=> form_input('input_des_lastname', '', 'class="form-control" required')
				,'INPUT_DES_PASSWD'			=> form_input('input_des_passwd', '', 'class="form-control" required')
				,'INPUT_DES_EMAIL'			=> form_input(['name' => 'input_des_email', 'type' => 'email'], '', 'class="form-control" ')
				,'SELECT_ID_TYPE_USER'		=> form_dropdown('select_id_type_user', $userTypes, '', 'id="select_id_type_user" class="form-control" required')
				,'SELECT_IND_STATUS'		=> form_dropdown('select_ind_activo', $this->config->item('OPTIONS_SINO'), 0, 'id="select_ind_activo" class="form-control" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'user', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editUser() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		if($ID <= 0x0000) {
			redirect(base_url() . 'user');
		}
		$u = $this->user->getUserRow(['id_user' => $ID]);
		if( $this->input->post() ) {
			$data = [
				'des_user'			=> $this->input->post('input_des_user', TRUE)
				,'des_firstname'	=> $this->input->post('input_des_firstname', TRUE)
				,'des_lastname'		=> $this->input->post('input_des_lastname', TRUE)
				,'des_email'		=> $this->input->post('input_des_email', TRUE)
				,'ind_status'		=> intval($this->input->post('select_ind_activo'))
				,'id_type_user'		=> $this->input->post('select_id_type_user')
			];
			if($this->input->post('input_des_passwd') != '') {
				$data['des_passwd']	= $this->input->post('input_des_passwd', TRUE);
			}
			if( $this->user->updateUser($data, $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'user/viewUser/' . $ID);
			}
		}
		$typeUsers = $this->user->getTypeUsers();
		$userTypes = [];
		foreach($typeUsers as $t) {
			$userTypes[$t->id_type_user] = $t->des_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('user/editUser', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Editar Usuario'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() .'user/editUser/' . $u->id_user
				,'INPUT_ID_USER'			=> form_hidden('input_id_user', $u->id_user, 'class="form-control" required')
				,'INPUT_DES_USER'			=> form_input('input_des_user', $u->des_user, 'class="form-control" required')
				,'INPUT_DES_FIRSTNAME'		=> form_input('input_des_firstname', $u->des_firstname, 'class="form-control" required')
				,'INPUT_DES_LASTNAME'		=> form_input('input_des_lastname', $u->des_lastname, 'class="form-control" required')
				,'INPUT_DES_PASSWD'			=> form_input('input_des_passwd', '', 'class="form-control" ')
				,'INPUT_DES_EMAIL'			=> form_input(['name' => 'input_des_email', 'type' => 'email'], $u->des_email, 'class="form-control" ')
				,'SELECT_ID_TYPE_USER'		=> form_dropdown('select_id_type_user', $userTypes, $u->id_type_user, 'id="select_id_type_user" class="form-control" required')
				,'SELECT_IND_STATUS'		=> form_dropdown('select_ind_activo', $this->config->item('OPTIONS_SINO'), $u->ind_status, 'id="select_ind_activo" class="form-control" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'user', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewUser() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		if($ID <= 0x0000) {
			redirect(base_url() . 'user');
		}
		$u = $this->user->getUserRow(['id_user' => $ID]);
		$tu = $this->user->getTypeUserRow(['id_type_user' => $u->id_type_user]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('user/viewUser', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Ver Usuario'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'INPUT_DES_USER'			=> form_input('input_des_user', $u->des_user, 'class="form-control" readonly')
				,'INPUT_DES_FIRSTNAME'		=> form_input('input_des_firstname', $u->des_firstname, 'class="form-control" readonly')
				,'INPUT_DES_LASTNAME'		=> form_input('input_des_lastname', $u->des_lastname, 'class="form-control" readonly')
				,'INPUT_DES_PASSWD'			=> form_input('input_des_passwd', '******', 'class="form-control" readonly')
				,'INPUT_DES_EMAIL'			=> form_input(['name' => 'input_des_email', 'type' => 'email'], $u->des_email, 'class="form-control" readonly')
				,'SELECT_ID_TYPE_USER'		=> form_input('select_id_type_user', $tu->des_name, 'class="form-control" readonly')
				,'SELECT_IND_STATUS'		=> form_input('select_ind_activo', $this->config->item('OPTIONS_SINO')[$u->ind_status], 'class="form-control" readonly')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'user', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteUser() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID <= 0x0001) {
			header('Location: ' . base_url() . 'user');
			exit();
		}
		$p = $this->user->getUserRow(['id_user' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_user', TRUE) == $ID) {
				if ($this->user->deleteUser($this->input->post('input_id_user', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'user');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Usuario ' . $p->des_user, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Eliminar Usuario - ' . $p->des_user
				,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
				,'BODY_MENU'				=> '(' . $p->des_lastname .', ' . $p->des_firstname . ') ' . $p->des_user
				,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Usuario "' . $p->des_user . '"'
				,'URL_POST'					=> base_url() . 'user/deleteUser/' . $ID
				,'INPUT_DELETE_ID'			=> form_hidden('input_id_user', $ID)
				,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'user', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getUsers() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_user' => $search];
				$result = $this->user->getUsers(null, $like);
				$total = $this->user->getTotalUser(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_user' => $search];
					$result = $this->user->getUsers(null, $like, $start, $limit);
					$total = $this->user->getTotalUser(null, $like);
				} else {
					$result = $this->user->getUsers(null, null, $start, $limit);
					$total = $this->user->getTotalUser();
				}
			}
		} else {
			$result = $this->user->getUsers();
		}
		switch($format) {
			case 'object':
				return $result;
				break;
			case 'array':
		
				break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						$t = $this->user->getTypeUserRow(['id_type_user' => $r->id_type_user]);
						array_push($records, [
								'id_user'			=> $r->id_user
								,'des_user'			=> $r->des_user
								,'des_firstname'	=> $r->des_firstname
								,'des_lastname'		=> $r->des_lastname
								,'des_email'		=> $r->des_email
								,'ind_status'		=> $this->config->item('OPTIONS_SINO')[$r->ind_status]
								,'id_type_user'		=> $t->des_name
						]);
					}
					$data = ['draw' => $draw
							,'recordsTotal' => $total
							,'recordsFiltered' => $total
							,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
				break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_perm
							,'name'		=> $k->des_name
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
				break;
			default:
		
				break;
		}
	}
	//BUILD SESSION
	public function buildSessionPerms() {
		$ID = (sizeof(func_get_args())== 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		$groupPerm = [];
		$userPerm = [];
		$gResult = $this->user->getGroupPerms(['id_type_user' => $this->session->userdata('USER')->id_type_user]);
		foreach($gResult as $p) {
			$r = $this->user->getPermRow(['id_perm' => $p->id_perm]);
			if($p->ind_add == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_add';
			if($p->ind_edit == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_edit';
			if($p->ind_del == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_del';
			if($p->ind_show == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_show';
		}
		$uResult = $this->user->getUserPerms(['id_user' => $this->session->userdata('USER')->id_user]);
		foreach($uResult as $u) {
			$r = $this->user->getPermRow(['id_perm' => $u->id_perm]);
			if($u->ind_add == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_add';
			if($u->ind_edit == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_edit';
			if($u->ind_del == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_del';
			if($u->ind_show == 0x0001)
				$groupPerm[strtolower($r->des_machine_name)][] = 'ind_show';
		}
		$_perm = array_merge($groupPerm, $userPerm);
		$perm = [];
		foreach($_perm as $k => $v) {
			$perm[$k] = array_unique($v);
		}
		$this->session->unset_userdata('PERMS');
		$this->session->set_userdata('PERMS', $perm);
	}
}
