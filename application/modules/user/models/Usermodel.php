<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userModel extends MX_Controller {
	
	const USER = 'gen_user';
	const TYPE_USER = 'gen_type_user';
	const PERMS = 'gen_perm';
	const USER_PERMS = 'gen_user_perm';
	const USER_GROUP_PERMS = 'gen_type_user_perm';
	
    function __construct()
    {
        parent::__construct();
    }
    

    function getUsers($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::USER)->result();
    }
    function getTotalUser($where = null, $like = null, $start = null, $limit = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($start, $limit);
    	$this->db->from(self::USER);
    	return $this->db->count_all_results();
    }
    function getUserRow($where) {
    	return $this->db->get_where(self::USER, $where)->row();
    }
    function saveUser($data) {
    	$this->db->insert(self::USER, $data);
		return $this->db->insert_id();
    }
    function updateUser($data, $ID) {
    	$this->db->trans_start();
    	$this->db->update(self::USER, $data, ['id_user' => $ID]);
    	$this->db->trans_complete();
    	if ($this->db->trans_status() === FALSE)
    		return FALSE;
    	else
    		return TRUE;
    }
    function deleteUser($ID) {
    	$this->db->trans_start();
    	$this->db->delete(self::USER, ['id_user' => $ID]);
    	$this->db->trans_complete();
    	if ($this->db->trans_status() === FALSE)
    		return FALSE;
    	else
    		return TRUE;
    }
    
    
    function getGroupPerms($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::USER_GROUP_PERMS)->result();
    }
    function getUserPerms($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::USER_PERMS)->result();
    }
    
    function getPermRow($where) {
    	return $this->db->get_where(self::PERMS, $where)->row();
    }
    function getTypeUserRow($where) {
    	return $this->db->get_where(self::TYPE_USER, $where)->row();
    }
    function getTypeUsers($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::TYPE_USER)->result();
    }
    
    /*
    function getUsuarios()
    {
        $query = $this->db->get(self::USUARIO)->result();
        return $query;
    }
    
    function getUsuarioByID($ID) {
    	return $this->db->get_where(self::USUARIO, ['id_usuario' => $ID])->row();
    }
	
	function saveUsuario($data) {
		$this->db->trans_start();
		$this->db->insert(self::USUARIO, $data);
		$this->db->trans_complete();
	}
	
	function updateUsuario($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::USUARIO, $data, ['id_usuario' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteUsuario($ID) {
		$this->db->trans_start();
		$this->db->delete(self::USUARIO_OPCIONES, ['id_usuario' => $ID]);
		$this->db->delete(self::USUARIO, ['id_usuario' => $ID]);
		$this->db->trans_complete();
	}
	
	function getMaxIDUsuario() {
		$this->db->select_max('id_usuario');
		return $this->db->get(self::USUARIO)->row()->id_usuario;
	}
	// OPCIONES
	
	
	function getTotalOpciones($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::OPCIONES);
		return $this->db->count_all_results();
	}
	
	function getOpciones($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		return $this->db->get(self::OPCIONES)->result();
	}
	// PERMISOS
	function getTotalPermisos($where = null, $like = null, $limit = null, $start = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		$this->db->from(self::USUARIO_OPCIONES);
		return $this->db->count_all_results();
	}
	
	function getPermisos($where = null, $like = null, $limit = null, $start = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get(self::USUARIO_OPCIONES)->result();
	}
	
	function savePermiso($data) {
		$this->db->trans_start();
		$this->db->insert(self::USUARIO_OPCIONES, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function updatePermiso($data, $where) {
		$this->db->trans_start();
		$this->db->update(self::USUARIO_OPCIONES, $data, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deletePermiso($where) {
		$this->db->trans_start();
		$this->db->delete(self::USUARIO_OPCIONES, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	// PERMISOS PROYECTO
	function getTotalPermisosProyecto($where = null, $like = null, $limit = null, $start = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		$this->db->from(self::USUARIO_PROYECTO);
		return $this->db->count_all_results();
	}
	
	function getPermisosProyecto($where = null, $like = null, $limit = null, $start = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get(self::USUARIO_PROYECTO)->result();
	}
	
	function savePermisoProyecto($data) {
		$this->db->trans_start();
		$this->db->insert(self::USUARIO_PROYECTO, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function updatePermisoProyecto($data, $where) {
		$this->db->trans_start();
		$this->db->update(self::USUARIO_PROYECTO, $data, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deletePermisoProyecto($where) {
		$this->db->trans_start();
		$this->db->delete(self::USUARIO_PROYECTO, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	*/
}
