<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/select2/css/select2.min.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/parsley/parsley.js"></script>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>
<div class="panel">
    <div class="panel-body">
		<div class="example-box-wrapper">
			<form method="post" action="{URL_POST}" enctype="multipart/form-data" data-parsley-validate>
				<div class="row">
                    <div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">USUARIO</label>
                            <div class="col-sm-6">
                                {INPUT_DES_USER}
                            </div>
						</div>
					</div>
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">CONTRASE&Ntilde;A</label>
                            <div class="col-sm-6">
                                {INPUT_DES_PASSWD}
                            </div>
						</div>
					</div>
				</div><br />
				<div class="row">
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">NOMBRES</label>
                            <div class="col-sm-6">
                                {INPUT_DES_FIRSTNAME}
                            </div>
						</div>
					</div>
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">APELLIDOS</label>
                            <div class="col-sm-6">
                                {INPUT_DES_LASTNAME}
                            </div>
						</div>
					</div>
				</div><br />
				<div class="row">
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">EMAIL</label>
                            <div class="col-sm-6">
                                {INPUT_DES_EMAIL}
                            </div>
						</div>
					</div>
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">TIPO DE USUARIO</label>
                            <div class="col-sm-6">
                                {SELECT_ID_TYPE_USER}
                            </div>
						</div>
					</div>
				</div><br />
				<div class="row">
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <label class="col-sm-3 control-label">ACTIVO</label>
                            <div class="col-sm-6">
                                {SELECT_IND_STATUS}
                            </div>
						</div>
					</div>
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
                            <div class="col-sm-6">
                                {BUTTON_SUBMIT}
                            </div>
                            <div class="col-sm-6">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div><br />
			</form>
		</div>
	</div>
</div>