<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/parsley/parsley.js"></script>

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/icon.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/easyui/extensions/jquery-easyui-edatagrid/jquery.edatagrid.js"></script>
<script type="text/javascript">
	$(function(){
		$('#dg').edatagrid({
			url: '{URL_GET_PERMISOS}'
			,saveUrl: '{URL_SAVE_PERMISOS}'
			,updateUrl: '{URL_UPDATE_PERMISOS}'
			,destroyUrl: '{URL_DESTROY_PERMISOS}'
		});
		$('#dg').edatagrid({
			onError: function(index,row){
				if(row.title)
					title = row.title;
				else 
					title = 'Error';
				$.messager.show({
					title: title,
					msg:row.msg,
					timeout:5000,
					showType:'slide'
				});
				$('#dg').edatagrid('reload');
			}
		});
	});
</script>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>
<div class="panel-B">
    <div class="panel-B-body">
		<div class="example-box-wrapper">
			<div class="row">
				<div class="col-md-12 form-horizontal">
					<table id="dg" title="Permisos" style="min-width:100px;min-height:250px"
							toolbar="#toolbar" pagination="true" idField="id_opcion"
							rownumbers="true" fitColumns="true" singleSelect="true">
						<thead>
							<tr>
								<th field="id_usuario" width="10" hidden="true">TAG</th>
								<th field="id_opcion" width="10" editor='{type:"combobox",options:{
					                   onBeforeLoad: function(param){
											if (!param || param.length<2) return false;
										}
					                   ,url: "{SELECT_ID_OPCION}"
					                    ,mode: "remote"
					                    ,valueField: "id"
					                    ,textField: "name"
										,required:true
				                    }
								}'>OPCION</th>
								<th field="ind_nuevo" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_NUEVO}
                                        ,required:true
                                    }
                                }'>NUEVO</th>
								<th field="ind_modifica" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_MODIFICA}
                                        ,required:true
                                    }
                                }'>MODIFICA</th>
								<th field="ind_elimina" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_ELIMINA}
                                        ,required:true
                                    }
                                }'>ELIMINAR</th>
								<th field="ind_consulta" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_CONSULTA}
                                        ,required:true
                                    }
                                }'>CONSULTA</th>
							</tr>
						</thead>
					</table>
					<div id="toolbar">
						<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dg').edatagrid('addRow')">Nuevo</a>
						<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dg').edatagrid('destroyRow')">Eliminar</a>
						<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dg').edatagrid('saveRow')">Grabar</a>
						<a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dg').edatagrid('cancelRow')">Cancelar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>