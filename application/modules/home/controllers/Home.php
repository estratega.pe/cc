<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('home/homeModel', 'home');
	}
	
	public function index()
	{
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME', 'BASE_URL' => base_url()];
		$data['BODY'] = '';
		renderPage($data);
	}
}
