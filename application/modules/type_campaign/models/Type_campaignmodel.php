<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class type_campaignModel extends MX_Controller {
	
	const TYPE_CAMPAIGN = 'gen_type_campaign';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getTypes($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::TYPE_CAMPAIGN)->result();
    }
    
    function getTypeRow($where) {
    	return $this->db->get_where(self::TYPE_CAMPAIGN, $where)->row();
    }
	
	function saveType($data) {
		$this->db->insert(self::TYPE_CAMPAIGN, $data);
		return $this->db->insert_id();
	}
	
	function updateType($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::TYPE_CAMPAIGN, $data, ['id_type_campaign' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteType($ID) {
		$this->db->trans_start();
		$this->db->delete(self::TYPE_CAMPAIGN, ['id_type_campaign' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalType($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::TYPE_CAMPAIGN);
		return $this->db->count_all_results();
	}
}
