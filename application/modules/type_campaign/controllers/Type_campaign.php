<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_campaign extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('type_campaign/type_campaignModel', 'type');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_type_campaign']
				,['data' => 'des_name']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'type_campaign/viewType_campaign/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'type_campaign/editType_campaign/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'type_campaign/deleteType_campaign/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Tipos de Campa&ntilde;as', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Tipos de Campa&ntilde;as'
			,'URL_AJAX'			=> base_url() . 'type_campaign/getTypes/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Tipos de Campa&ntilde;as'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_type_campaign'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'type_campaign/addType_campaign', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addType_campaign() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->type->savetype(['des_name' => $this->input->post('input_des_name', TRUE)]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'type_campaign/viewType_campaign/' . $newID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Tipo de Campa&ntilde;a', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('type_campaign/addType_campaign', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Tipo de Campa&ntilde;a'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'type_campaign/addType_campaign/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'type_campaign', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editType_campaign() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->type->getTypeRow(['id_type_campaign' => $ID]);
	if( $this->input->post() ) {
			$data = ['des_name'			=> $this->input->post('input_des_name', TRUE)];
			if( $this->type->updateType($data, $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'type_campaign/viewType_campaign/' . $ID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Tipo de Campa&ntilde;a', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('type_campaign/editType_campaign', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Tipo de Campa&ntilde;a'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'type_campaign/editType_campaign/' . $p->id_type_campaign
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_ID_PERM'			=> form_hidden('input_id_type_campaign', $p->id_type_campaign)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'type_campaign', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewType_campaign() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'type_campaign');
			exit();
		}
		$p = $this->type->getTypeRow(['id_type_campaign' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Tipo de Campa&ntilde;a', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('type_campaign/viewType_campaign', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Tipo de Campa&ntilde;a'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'type_campaign', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteType_campaign() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'type_campaign');
			exit();
		}
		$p = $this->type->getTypeRow(['id_type_campaign' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_type_campaign', TRUE) == $ID) {
				if ($this->type->deleteType($this->input->post('input_id_type_campaign', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'type_campaign');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Tipo de Campa&ntilde;a ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Tipo de Campa&ntilde;a - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->id_type_campaign . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Tipo de Campa&ntilde;a "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'type_campaign/deleteType_campaign/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_type_campaign', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'type_campaign', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getTypes() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->type->getTypess(null, $like);
				$total = $this->type->getTotalType(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->type->getTypes(null, $like, $start, $limit);
					$total = $this->type->getTotalType(null, $like);
				} else {
					$result = $this->type->getTypes(null, null, $start, $limit);
					$total = $this->type->getTotalType();
				}
			}
		} else {
			$result = $this->type->getTypes();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_type_campaign'			=> $r->id_type_campaign
							,'des_name'			=> $r->des_name
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_type_campaign
							,'name'		=> $k->des_name
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
			break;
			default:
				
			break;
		}
	}
}
