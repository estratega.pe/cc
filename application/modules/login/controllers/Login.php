<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('login/loginModel', 'login');
	}
	
	public function index()
	{
		if($this->session->userdata('USER'))
			redirect(base_url());
		$body['PAGE_LOGIN_TITLE'] = 'Ingresar al sistema';
		$body['URL_POST'] = base_url() .'login/loginIn';
		$body['BASE_URL'] = base_url();
		$this->parser->parse('login/login', $body);
	}
	
	public function loginIn()
	{
		if($this->input->post())
		{
			$usuario	= $this->security->xss_clean($this->input->post('usuario', TRUE));
			$contrasena	= $this->security->xss_clean($this->input->post('contrasena', TRUE));
			if(trim($usuario) == '' || trim($contrasena) == '') {
				redirect(base_url('login'));
			}
			$_usuario = $this->login->loginIn(['des_user' => $usuario, 'des_passwd' => $contrasena, 'ind_status' => 1]);
			if(sizeof($_usuario) != 0x0001)
				redirect(base_url('login'));
			$this->session->set_userdata('USER', $_usuario);
			modules::run('user/buildSessionPerms', $_usuario->id_user);
			redirect(base_url());
		} else {
			redirect(base_url('login'));
		}
	}
	
	public function loginOut()
	{
		$this->session->unset_userdata('USER');
		$this->session->unset_userdata('PERMS');
		redirect(base_url('login'));
	}
}
