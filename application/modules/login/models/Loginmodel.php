<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loginModel extends MX_Controller {
	
	const USER = 'gen_user';
     
    function __construct()
    {
        parent::__construct();
    }
     
    function loginIn($where)
    {
        return $this->db->get_where(self::USER, $where)->row();
    }
	
}
