<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('agent/agentModel', 'agent');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data'		=> 'id_agent']
				,['data'	=> 'des_name']
				,['data'	=> 'turno']
				,['data'	=> 'des_machine_name']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'agent/viewAgent/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'agent/editAgent/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'agent/deleteAgent/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Agentes', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Agentes'
			,'URL_AJAX'			=> base_url() . 'agent/getAgents/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Agentes'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_agent'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'agent/addAgent', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'TURNO'], ['LABEL' => 'KEY'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addAgent() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->agent->getMaxIDAgent() + 1;
			$values = ['des_name' => $this->input->post('input_des_name', TRUE)
					,'des_machine_name' => 'agent' . $newID
			];
			$idUser = $this->input->post('select_id_user');
			$idTurn = $this->input->post('select_id_turn');
			$newID = $this->agent->saveAgent($values, $idUser, $idTurn);
			if(  is_numeric($newID) ) {
				regenerateAgents();
				regenerateSip();
				redirect(base_url() . 'agent/viewAgent/' . $newID);
			}
		}
		$p_select_user = [];
		$users = $this->agent->getUsers();
		$selected_users = $this->agent->selectedUser();
		$noUser = [];
		foreach($selected_users as $nU) {
			$noUser[] = $nU->id_user;
		}
		foreach($users as $k) {
			if(!in_array($k->id_user, $noUser)) {
				$p_select_user[$k->id_user] = $k->des_user;
			}
		}
		$p_select_turn = [];
		$turns = $this->agent->getTurns();
		foreach($turns as $k) {
			$p_select_turn[$k->id_turn] = $k->des_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Agente', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('agent/addAgent', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Agente'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'agent/addAgent/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'SELECT_ID_USER'			=> form_dropdown('select_id_user', $p_select_user, '', 'class="form-control" required')
			,'SELECT_ID_TURN'			=> form_dropdown('select_id_turn', $p_select_turn, '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'agent', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editAgent() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->agent->getAgentRow(['id_agent' => $ID]);
		$uA = $this->agent->getUserAgentRow(['id_agent' => $ID]);
		if( $this->input->post() ) {
			$values = ['des_name'=> $this->input->post('input_des_name', TRUE)];
			$user = $this->input->post('select_id_user');
			$turn = $this->input->post('select_id_turn');
			$data = ['id_user' => $user, 'id_agent' => $ID, 'id_turn' => $turn];
			$where = ['id_user' => $uA->id_user, 'id_agent' => $uA->id_agent, 'id_turn' => $uA->id_turn];
			if( $this->agent->updateAgent($values, $ID, $data, $where) === FALSE) {
				$this->db->trans_rollback();
			} else {
				regenerateAgents();
				regenerateSip();
				redirect(base_url() . 'agent/viewAgent/' . $ID);
			}
		}
		$p_select_user = [];
		$user = $this->agent->getUserRow(['id_user' => $uA->id_user]);
		$p_select_user[$user->id_user] = $user->des_user;
		$users = $this->agent->getUsers();
		$selected_users = $this->agent->selectedUser();
		$noUser = [];
		foreach($selected_users as $nU) {
			$noUser[] = $nU->id_user;
		}
		foreach($users as $k) {
			if(!in_array($k->id_user, $noUser)) {
				$p_select_user[$k->id_user] = $k->des_user;
			}
		}
		$p_select_turn = [];
		$turns = $this->agent->getTurns();
		foreach($turns as $k) {
			$p_select_turn[$k->id_turn] = $k->des_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Agente', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('agent/editAgent', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Agente'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'agent/editAgent/' . $p->id_agent
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_ID_PERM'			=> form_hidden('input_id_agent', $p->id_agent)
			,'SELECT_ID_USER'			=> form_dropdown('select_id_user', $p_select_user, $uA->id_user, 'class="form-control" required')
			,'SELECT_ID_TURN'			=> form_dropdown('select_id_turn', $p_select_turn, $uA->id_turn, 'class="form-control" required')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'agent', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewAgent() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'agent');
			exit();
		}
		$p = $this->agent->getAgentRow(['id_agent' => $ID]);
		$uA = $this->agent->getUserAgentRow(['id_agent' => $ID]);
		$user = $this->agent->getUserRow(['id_user' => $uA->id_user]);
		$turn = $this->agent->getTurnRow(['id_turn' => $uA->id_turn]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Agente', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('agent/viewAgent', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Agente'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'INPUT_ID_USER'			=> form_input('input_id_user', $user->des_user, 'class="form-control" readonly')
			,'INPUT_ID_TURN'			=> form_input('input_id_turn', $turn->des_name, 'class="form-control" readonly')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'agent', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteAgent() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'agent');
			exit();
		}
		$p = $this->agent->getAgentRow(['id_agent' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_agent', TRUE) == $ID) {
				if ($this->agent->deleteAgent($this->input->post('input_id_agent', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					regenerateAgents();
					redirect(base_url() . 'agent');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Agente ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Agente - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->des_machine_name . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Agente "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'agent/deleteAgent/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_agent', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'agent', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getAgents() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->agent->getAgents(null, $like);
				$total = $this->agent->getTotalAgent(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->agent->getAgents(null, $like, $start, $limit);
					$total = $this->agent->getTotalAgent(null, $like);
				} else {
					$result = $this->agent->getAgents(null, null, $start, $limit);
					$total = $this->agent->getTotalAgent();
				}
			}
		} else {
			$result = $this->agent->getAgents();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						$uA = $this->agent->getUserAgentRow(['id_agent' => $r->id_agent]);
						$u = $this->agent->getUserRow(['id_user' => $uA->id_user]);
						$t = $this->agent->getTurnRow(['id_turn' => $uA->id_turn]);
						array_push($records, [
							'id_agent'			=> $r->id_agent
							,'des_name'			=> $r->des_name
							,'turno'			=> $t->des_name
							,'usuario'			=> $u->des_user
							,'des_machine_name'	=> $r->des_machine_name
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_agent
							,'name'		=> $k->des_name
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
			break;
			default:
				
			break;
		}
	}
}
