<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class agentModel extends MX_Controller {
	
	const AGENT = 'gen_agent';
	const USER = 'gen_user';
	const USER_AGENT = 'gen_user_agent';
	const TURN = 'gen_turn';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getMaxIDAgent() {
    	$this->db->select_max('id_agent');
    	return $this->db->get(self::AGENT)->row()->id_agent;
    }
    
    function getAgents($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::AGENT)->result();
    }
    
    function getAgentRow($where) {
    	return $this->db->get_where(self::AGENT, $where)->row();
    }
	#UserAgent
	function saveAgent($data, $USER, $TURN) {
		$this->db->trans_start();
		$this->db->insert(self::AGENT, $data);
		$ID = $this->db->insert_id();
		$this->db->insert(self::USER_AGENT, ['id_user' => $USER, 'id_agent' => $ID, 'id_turn' => $TURN]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return $ID;
	}
	
	function updateAgent($data, $ID, $values, $where) {
		$this->db->trans_start();
		$this->db->update(self::USER_AGENT, $values, $where);
		$this->db->update(self::AGENT, $data, ['id_agent' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteAgent($ID) {
		$this->db->trans_start();
		$this->db->delete(self::USER_AGENT, ['id_agent' => $ID]);
		$this->db->delete(self::AGENT, ['id_agent' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalAgent($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::AGENT);
		return $this->db->count_all_results();
	}
	
	#Users
	function getUserRow($where) {
		return $this->db->get_where(self::USER, $where)->row();
	}
	public function getUsers($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get(self::USER)->result();
	}
	#UserAgent
	function getUserAgentRow($where) {
		return $this->db->get_where(self::USER_AGENT, $where)->row();
	}
	public function selectedUser($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get(self::USER_AGENT)->result();
	}
	#Turn
	#UserAgent
	function getTurnRow($where) {
		return $this->db->get_where(self::TURN, $where)->row();
	}
	public function getTurns($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get(self::TURN)->result();
	}
}
