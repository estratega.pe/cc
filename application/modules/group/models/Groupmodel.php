<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class groupModel extends MX_Controller {
	
	const GROUPS = 'gen_type_user';
	const PERM = 'gen_perm';
	const PERMS = 'gen_type_user_perm';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getGroups($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::GROUPS)->result();
    }
    
    function getGroupRow($where) {
    	return $this->db->get_where(self::GROUPS, $where)->row();
    }
	
	function saveGroup($data) {
		$this->db->insert(self::GROUPS, $data);
		return $this->db->insert_id();
	}
	
	function updateGroup($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::GROUPS, $data, ['id_type_user' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteGroup($ID) {
		$this->db->trans_start();
		$this->db->delete(self::GROUPS, ['id_type_user' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalGroup($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::GROUPS);
		return $this->db->count_all_results();
	}
	#PERMS
	function getPermRow($where) {
		return $this->db->get_where(self::PERM, $where)->row();
	}
	function getPerms($where = null, $like = null, $limit = null, $start = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get(self::PERMS)->result();
	}
	function getTotalPerms($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::PERMS);
		return $this->db->count_all_results();
	}
	function savePerm($data) {
		$this->db->trans_start();
		$this->db->insert(self::PERMS, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	function updatePerm($data, $where) {
		$this->db->trans_start();
		$this->db->update(self::PERMS, $data, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	function deletePerm($where) {
		$this->db->trans_start();
		$this->db->delete(self::PERMS, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
}
