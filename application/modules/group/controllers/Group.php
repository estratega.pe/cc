<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('group/groupModel', 'group');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_type_user']
				,['data' => 'des_name']
				,['data' => 'des_machine_name']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'group/viewGroup/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'group/editGroup/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'group/deleteGroup/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Tipos de Usuarios', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Tipos de Usuarios'
			,'URL_AJAX'			=> base_url() . 'group/getGroups/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Tipos de Usuarios'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_type_user'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'group/addGroup', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'KEY'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addGroup() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->group->saveGroup(['des_name' => $this->input->post('input_des_name', TRUE), 'des_machine_name' => $this->input->post('input_des_machine_name', TRUE)]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'group/viewGroup/' . $newID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Tipo de Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('group/addGroup', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Tipo de Usuario'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'group/addGroup/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'group', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editGroup() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->group->getGroupRow(['id_type_user' => $ID]);
		if( $this->input->post() ) {
			if( $this->group->updateGroup(['des_name'=> $this->input->post('input_des_name', TRUE)], $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'group/viewGroup/' . $ID);
			}
		}
		$p_select_ind_activo = [];
		$opciones_sino = $this->config->item('OPTIONS_SINO');
		while ($name = current($opciones_sino)) {
			array_push($p_select_ind_activo, ['ID' => key($opciones_sino), 'NAME' => $name]);
			next($opciones_sino);
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Tipo de Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('group/editGroup', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Tipo de Usuario'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'group/editGroup/' . $p->id_type_user
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_ID_GROUP'			=> form_hidden('input_id_type_user', $p->id_type_user)
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'group', 'btn-danger', 'icon-ban', 'Cancelar', true)
				,'SELECT_IND_ADD'		=> json_encode($p_select_ind_activo)
				,'SELECT_IND_EDIT'	=> json_encode($p_select_ind_activo)
				,'SELECT_IND_DEL'	=> json_encode($p_select_ind_activo)
				,'SELECT_IND_SHOW'	=> json_encode($p_select_ind_activo)
				,'SELECT_ID_OPCION'	=> base_url() . 'perm/getPerms/json-ui'
				,'URL_GET_PERMS'	=> base_url() . 'group/getPerms/json-ui/' . $p->id_type_user
				,'URL_SAVE_PERM'	=> base_url() . 'group/savePerm/' . $p->id_type_user
				,'URL_UPDATE_PERM'	=> base_url() . 'group/updatePerm/' . $p->id_type_user
				,'URL_DESTROY_PERM'	=> base_url() . 'group/destroyPerm/' . $p->id_type_user
		], TRUE);
		renderPage($data);
	}
	
	public function viewGroup() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'group');
			exit();
		}
		$p = $this->group->getGroupRow(['id_type_user' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Tipo de Usuario', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('group/viewGroup', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Tipo de Usuario'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'group', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteGroup() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 || $ID <= 3) {
			header('Location: ' . base_url() . 'group');
			exit();
		}
		$p = $this->group->getGroupRow(['id_type_user' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_type_user', TRUE) == $ID) {
				if ($this->group->deleteGroup($this->input->post('input_id_type_user', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'group');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Tipo de Usuario ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Tipo de Usuario - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->des_machine_name . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Tipo de Usuario "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'group/deleteGroup/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_type_user', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'group', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getGroups() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->group->getGroups(null, $like);
				$total = $this->group->getTotalGroup(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->group->getPerms(null, $like, $start, $limit);
					$total = $this->group->getTotalGroup(null, $like);
				} else {
					$result = $this->group->getGroups(null, null, $start, $limit);
					$total = $this->group->getTotalGroup();
				}
			}
		} else {
			$result = $this->group->getGroups();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_type_user'		=> $r->id_type_user
							,'des_name'			=> $r->des_name
							,'des_machine_name'	=> $r->des_machine_name
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				
			break;
		}
	}
	
	public function getPerms() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(4);
		$page = 1;
		$rows = 10;
		$start = 0;
		if($this->input->post()) {
			$start = ($this->input->post('page') == '') ? $page : $this->input->post('page');
			$rows = ($this->input->post('rows') == '') ? $rows : $this->input->post('rows');
			$start = ($start - $page) * $rows;
		}
		$result = $this->group->getPerms(['id_type_user' => $ID], null, $rows, $start);
		switch($format) {
			case 'json-ui':
				$row = [];
				foreach($result as $k) {
					$r = $this->group->getPermRow(['id_perm' => $k->id_perm]);
					array_push($row, [
						'id_type_user'	=> $ID
						,'id_perm_id'	=> $r->id_perm
						,'id_perm'		=> $r->des_name
						,'ind_add'		=> (is_null($k->ind_add)) ? '' : $this->config->item('OPTIONS_SINO')[$k->ind_add]
						,'ind_edit'		=> (is_null($k->ind_edit)) ? '' : $this->config->item('OPTIONS_SINO')[$k->ind_edit]
						,'ind_del'		=> (is_null($k->ind_del)) ? '' : $this->config->item('OPTIONS_SINO')[$k->ind_del]
						,'ind_show'		=> (is_null($k->ind_show)) ? '' : $this->config->item('OPTIONS_SINO')[$k->ind_show]
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( ['total' => $this->group->getTotalPerms(['id_type_user' => $ID]), 'rows' => $row] ));
			break;
			default:
				
			break;
		}
	}
	
	public function savePerm() {
		if(!acl('ind_add', 'perm')) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No tiene permisos suficientes']));
		}
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$id_perm = $this->input->post('id_perm', TRUE);
		$ind_add = $this->input->post('ind_add', TRUE);
		$ind_edit = $this->input->post('ind_edit', TRUE);
		$ind_del = $this->input->post('ind_del', TRUE);
		$ind_show = $this->input->post('ind_show', TRUE);
		$values = ['id_type_user'	=> $ID
				,'id_perm'			=> intval($id_perm)
				,'ind_add'			=> intval($ind_add)
				,'ind_edit'			=> intval($ind_edit)
				,'ind_del'			=> intval($ind_del)
				,'ind_show'			=> intval($ind_show)
		];
		if($this->group->savePerm($values) === FALSE) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No se ha podido guardar, intentelo mas tarde']));
		} else {
			if($this->session->userdata('USER')->id_type_user == $ID)
				modules::run('user/buildSessionPerms', $this->session->userdata('USER')->id_user);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'title' => 'Correcto', 'msg' => 'Grabado correctamente']));
		}
	}
	public function updatePerm() {
		if(!acl('ind_edit', 'perm')) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No tiene permisos suficientes']));
		}
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$id_perm_id = $this->input->post('id_perm_id', TRUE);
		$id_perm = $this->input->post('id_perm', TRUE);
		$ind_add = $this->input->post('ind_add', TRUE);
		$ind_edit = $this->input->post('ind_edit', TRUE);
		$ind_del = $this->input->post('ind_del', TRUE);
		$ind_show = $this->input->post('ind_show', TRUE);
		$where['id_type_user'] = $ID;
		$where['id_perm'] = $id_perm_id;
		if(is_numeric($id_perm)) {
			$values['id_perm'] = intval($id_perm);
		}
		if(is_numeric($ind_add)) {
			$values['ind_add']	= intval($ind_add);
		}
		if(is_numeric($ind_edit)) {
			$values['ind_edit']	= intval($ind_edit);
		}
		if(is_numeric($ind_del)) {
			$values['ind_del']	= intval($ind_del);
		}
		if(is_numeric($ind_show)) {
			$values['ind_show']	= intval($ind_show);
		}
		if($this->group->updatePerm($values, $where) === FALSE) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No se ha podido guardar, intentelo mas tarde']));
		} else {
			if($this->session->userdata('USER')->id_type_user == $ID)
				modules::run('user/buildSessionPerms', $this->session->userdata('USER')->id_user);
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode(['isError' => true, 'title' => 'Correcto', 'msg' => 'Actualizado correctamente']));
		}
	}
	public function destroyPerm() {
		if(!acl('ind_del', 'perm')) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No tiene permisos suficientes']));
		}
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$id_perm = $this->input->post('id', TRUE);
		if($this->group->deletePerm(['id_type_user' => $ID, 'id_perm' => $id_perm]) === FALSE) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['isError' => true, 'msg' => 'No se ha podido eliminar, intentelo mas tarde']));
		} else {
			if($this->session->userdata('USER')->id_type_user == $ID)
				modules::run('user/buildSessionPerms', $this->session->userdata('USER')->id_user);
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode(['isError' => true, 'title' => 'Correcto', 'msg' => 'Eliminado correctamente']));
		}
	}
}
