<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/parsley/parsley.js"></script>

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/icon.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/easyui/extensions/jquery-easyui-edatagrid/jquery.edatagrid.js"></script>
<script type="text/javascript">
$(function(){
	$('#dg').edatagrid({
		url: '{URL_GET_PERMS}'
		,saveUrl: '{URL_SAVE_PERM}'
		,updateUrl: '{URL_UPDATE_PERM}'
		,destroyUrl: '{URL_DESTROY_PERM}'
		,onError: function(index,row){
			if(row.title)
				title = row.title;
			else 
				title = 'Error';
			$.messager.show({
				title: title,
				msg:row.msg,
				timeout:5000,
				showType:'slide'
			});
			$('#dg').edatagrid('reload');
		}
	});
});
</script>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>
<div class="panel-B">
    <div class="panel-B-body">
		<div class="example-box-wrapper">
			<form class="form-horizontal" method="post" action="{URL_POST}" enctype="multipart/form-data" data-parsley-validate>
				<div class="row">
                    <div class="form-group">
	                    <label class="col-sm-3 control-label">PERMISO</label>
	                    <div class="col-sm-6">
	                        {INPUT_DES_NAME}
	                        {INPUT_ID_GROUP}
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-sm-3 control-label">PERMISO KEY</label>
	                    <div class="col-sm-6">
	                        {INPUT_DES_MACHINE_NAME}
	                    </div>
	                </div>
				</div><br />
				<div class="row">
					<div class="col-md-12 form-horizontal">
						<div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-4">
                                {BUTTON_SUBMIT}
                            </div>
                            <div class="col-sm-4">
                                {BUTTON_CANCEL}
                            </div>
                            <div class="col-sm-2">
                            </div>
						</div>
					</div>
				</div><br />
			</form>
		</div>
	</div>
</div>

<div class="panel-B">
    <div class="panel-B-body">
		<div class="example-box-wrapper">
			<div class="row">
				<div class="col-md-12 form-horizontal">
					<table id="dg" title="Permisos" style="min-width:100px;min-height:250px"
							toolbar="#toolbar" pagination="true" idField="id_perm_id"
							rownumbers="true" fitColumns="true" singleSelect="true">
						<thead>
							<tr>
								<th field="id_perm" width="10" editor='{type:"combobox",options:{
					                   onBeforeLoad: function(param){
											if (!param || param.length<2) return false;
										}
					                   ,url: "{SELECT_ID_OPCION}"
					                    ,mode: "remote"
					                    ,valueField: "id"
					                    ,textField: "name"
										,required:true
				                    }
								}'>PERMISO</th>
								<th field="ind_add" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_ADD}
                                        ,required:true
                                    }
                                }'>AGREGAR</th>
								<th field="ind_edit" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_EDIT}
                                        ,required:true
                                    }
                                }'>EDITAR</th>
								<th field="ind_del" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_DEL}
                                        ,required:true
                                    }
                                }'>ELIMINAR</th>
								<th field="ind_show" width="10" editor='{type:"combobox",options:{
                                        valueField:"ID"
                                        ,textField:"NAME"
                                        ,data:{SELECT_IND_SHOW}
                                        ,required:true
                                    }
                                }'>VER</th>
							</tr>
						</thead>
					</table>
					<div id="toolbar">
						<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dg').edatagrid('addRow')">Nuevo</a>
						<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dg').edatagrid('destroyRow')">Eliminar</a>
						<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dg').edatagrid('saveRow')">Grabar</a>
						<a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dg').edatagrid('cancelRow')">Cancelar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>