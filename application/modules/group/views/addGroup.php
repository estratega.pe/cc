<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/parsley/parsley.js"></script>
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">
<script type="text/javascript">
<!--
$(function () {

	$('.textarea-counter-300').keyup(function () {
      var max = 300;
      var len = $(this).val().length;
      if (len >= max) {
        $('.character-remaining-300').text(' Maximo de caracteres permitidos(300)');
      } else {
        var char = max - len;
        $('.character-remaining-300').text(char + ' caracteteres restantes');
      }
    });

});
//-->
</script>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>
<div class="panel-B">
    <div class="panel-B-body">
		<div class="example-box-wrapper">
			<form class="form-horizontal" method="post" action="{URL_POST}" enctype="multipart/form-data" data-parsley-validate>
				<div class="row">
                    <div class="form-group">
	                    <label class="col-sm-3 control-label">PERMISO</label>
	                    <div class="col-sm-6">
	                        {INPUT_DES_NAME}
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-sm-3 control-label">PERMISO KEY</label>
	                    <div class="col-sm-6">
	                        {INPUT_DES_MACHINE_NAME}
	                    </div>
	                </div>
				</div><br />
				<div class="row">
					<div class="col-md-12 form-horizontal">
						<div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-4">
                                {BUTTON_SUBMIT}
                            </div>
                            <div class="col-sm-4">
                                {BUTTON_CANCEL}
                            </div>
                            <div class="col-sm-2">
                            </div>
						</div>
					</div>
				</div><br />
			</form>
		</div>
	</div>
</div>