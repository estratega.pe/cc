<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turn extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('turn/turnModel', 'turn');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_turn']
				,['data' => 'des_name']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'turn/viewTurn/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'turn/editTurn/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'turn/deleteTurn/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Turnos', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Turnos'
			,'URL_AJAX'			=> base_url() . 'turn/getTurns/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Turnos'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_turn'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'turn/addTurn', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addTurn() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->turn->saveTurn(['des_name' => $this->input->post('input_des_name', TRUE)]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'turn/viewTurn/' . $newID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Turno', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('turn/addTurn', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Turno'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'turn/addTurn/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'turn', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editTurn() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->turn->getTurnRow(['id_turn' => $ID]);
		if( $this->input->post() ) {
			if( $this->turn->updateTurn(['des_name'=> $this->input->post('input_des_name', TRUE)], $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'turn/viewTurn/' . $ID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Turno', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('turn/editTurn', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Turno'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'turn/editTurn/' . $p->id_turn
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_ID_PERM'			=> form_hidden('input_id_turn', $p->id_turn)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'turn', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewTurn() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'turn');
			exit();
		}
		$p = $this->turn->getTurnRow(['id_turn' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Turno', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('turn/viewTurn', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Turno'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'turn', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteTurn() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'turn');
			exit();
		}
		$p = $this->turn->getTurnRow(['id_turn' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_turn', TRUE) == $ID) {
				if ($this->turn->deleteTurn($this->input->post('input_id_turn', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'turn');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Turno ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Turno - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->id_turn . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Turno "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'turn/deleteTurn/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_turn', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'turn', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getTurns() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->turn->getTurns(null, $like);
				$total = $this->turn->getTotalTurn(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->turn->getTurns(null, $like, $start, $limit);
					$total = $this->turn->getTotalTurn(null, $like);
				} else {
					$result = $this->turn->getTurns(null, null, $start, $limit);
					$total = $this->turn->getTotalTurn();
				}
			}
		} else {
			$result = $this->turn->getTurns();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_turn'			=> $r->id_turn
							,'des_name'			=> $r->des_name
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_turn
							,'name'		=> $k->des_name
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
			break;
			default:
				
			break;
		}
	}
}
