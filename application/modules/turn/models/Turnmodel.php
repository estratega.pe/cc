<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class turnModel extends MX_Controller {
	
	const TURN = 'gen_turn';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getTurns($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::TURN)->result();
    }
    
    function getTurnRow($where) {
    	return $this->db->get_where(self::TURN, $where)->row();
    }
	
	function saveTurn($data) {
		$this->db->insert(self::TURN, $data);
		return $this->db->insert_id();
	}
	
	function updateTurn($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::TURN, $data, ['id_turn' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteTurn($ID) {
		$this->db->trans_start();
		$this->db->delete(self::TURN, ['id_turn' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalTurn($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::TURN);
		return $this->db->count_all_results();
	}
}
