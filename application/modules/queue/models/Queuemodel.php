<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class queueModel extends MX_Controller {
	
	const QUEUE = 'gen_queue';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getQueues($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::QUEUE)->result();
    }
    
    function getQueueRow($where) {
    	return $this->db->get_where(self::QUEUE, $where)->row();
    }
	
	function saveQueue($data) {
		$this->db->insert(self::QUEUE, $data);
		return $this->db->insert_id();
	}
	
	function updateQueue($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::QUEUE, $data, ['id_queue' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteQueue($ID) {
		$this->db->trans_start();
		$this->db->delete(self::QUEUE, ['id_queue' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalQueue($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::QUEUE);
		return $this->db->count_all_results();
	}
}
