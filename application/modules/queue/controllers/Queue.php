<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queue extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('queue/queueModel', 'queue');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_queue']
				,['data' => 'des_name']
				,['data' => 'des_machine_name']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'queue/viewQueue/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'queue/editQueue/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'queue/deleteQueue/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Colas', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Colas'
			,'URL_AJAX'			=> base_url() . 'queue/getQueues/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Colas'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_queue'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'queue/addQueue', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'KEY'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addQueue() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->queue->saveQueue(['des_name' => $this->input->post('input_des_name', TRUE), 'des_machine_name' => $this->input->post('input_des_machine_name', TRUE)]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'queue/viewQueue/' . $newID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Cola', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('queue/addQueue', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Cola'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'queue/addQueue/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'queue', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editQueue() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->queue->getQueueRow(['id_queue' => $ID]);
		if( $this->input->post() ) {
			if( $this->queue->updateQueue(['des_name'=> $this->input->post('input_des_name', TRUE)], $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'queue/viewQueue/' . $ID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Cola', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('queue/editQueue', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Cola'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'queue/editQueue/' . $p->id_queue
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_ID_PERM'			=> form_hidden('input_id_queue', $p->id_queue)
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'queue', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewQueue() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'queue');
			exit();
		}
		$p = $this->queue->getQueueRow(['id_queue' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Cola', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('queue/viewQueue', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Cola'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'INPUT_DES_MACHINE_NAME'	=> form_input('input_des_machine_name', $p->des_machine_name, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'queue', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteQueue() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'queue');
			exit();
		}
		$p = $this->queue->getQueueRow(['id_queue' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_queue', TRUE) == $ID) {
				if ($this->queue->deleteQueue($this->input->post('input_id_queue', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'queue');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Cola ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Cola - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->des_machine_name . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion de la Cola "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'queue/deleteQueue/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_queue', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'queue', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getQueues() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->queue->getQueues(null, $like);
				$total = $this->queue->getTotalQueue(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->queue->getQueues(null, $like, $start, $limit);
					$total = $this->queue->getTotalQueue(null, $like);
				} else {
					$result = $this->queue->getQueues(null, null, $start, $limit);
					$total = $this->queue->getTotalQueue();
				}
			}
		} else {
			$result = $this->queue->getQueues();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_queue'			=> $r->id_queue
							,'des_name'			=> $r->des_name
							,'des_machine_name'	=> $r->des_machine_name
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_queue
							,'name'		=> $k->des_name
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
			break;
			default:
				
			break;
		}
	}
}
