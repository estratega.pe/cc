<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('client/clientModel', 'client');
	}
	
	public function index()
	{
		acl('ind_show', null, 403);
		
		$columnas = [
				['data' => 'id_client']
				,['data' => 'des_name']
				,['data' => 'des_ruc']
		];
		$show = (!acl('ind_show')) ? '' : createLink(base_url() . 'client/viewClient/\'+data+\'', 'btn-info', 'icon-eye', 'Ver');
		$edit = (!acl('ind_edit')) ? '' : '&nbsp;' . createLink(base_url() . 'client/editClient/\'+data+\'', 'btn-success', 'icon-edit', 'Editar');
		$del = (!acl('ind_del')) ? '' : '&nbsp;' . createLink(base_url() . 'client/deleteClient/\'+data+\'', 'btn-danger', 'icon-close', 'Eliminar');
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Clientes', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Clientes'
			,'URL_AJAX'			=> base_url() . 'client/getClients/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Clientes'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'id_client'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> (!acl('ind_add')) ? '' : createLink(base_url() . 'client/addClient', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'LINKS_ACTIONS'	=> $show . $edit . $del
			,'TH_TABLE'			=> [['LABEL' => 'ID'], ['LABEL' => 'NOMBRE'], ['LABEL' => 'RUC'], ['LABEL' => 'ACCIONES']]
		], TRUE);
		renderPage($data);
	}
	
	public function addClient() {
		acl('ind_add', null, 403);
		if( $this->input->post() ) {
			$newID = $this->client->saveClient([
						'des_name' => $this->input->post('input_des_name', TRUE)
						,'des_ruc' => $this->input->post('input_des_ruc', TRUE)
						,'des_address' => $this->input->post('input_des_address', TRUE)
						,'des_phone' => $this->input->post('input_des_phone', TRUE)
					]);
			if(  is_numeric($newID) ) {
				redirect(base_url() . 'client/viewClient/' . $newID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Cliente', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('client/addClient', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Nuevo Cliente'
			,'BODY_SUBTITLE'			=> ''
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'client/addClient/'
			,'INPUT_DES_NAME'			=> form_input('input_des_name', '', 'class="form-control" required')
			,'INPUT_DES_RUC'			=> form_input('input_des_ruc', '', 'class="form-control" required')
			,'INPUT_DES_ADDRESS'		=> form_input('input_des_address', '', 'class="form-control" required')
			,'INPUT_DES_PHONE'			=> form_input('input_des_phone', '', 'class="form-control" required')
			,'BUTTON_SUBMIT'			=> createSubmitButton('Crear', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'client', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function editClient() {
		acl('ind_edit', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$p = $this->client->getClientRow(['id_client' => $ID]);
		if( $this->input->post() ) {
			$data = [
				'des_name'			=> $this->input->post('input_des_name', TRUE)
				,'des_ruc'			=> $this->input->post('input_des_ruc', TRUE)
				,'des_address'		=> $this->input->post('input_des_address', TRUE)
				,'des_phone'		=> $this->input->post('input_des_phone', TRUE)
			];
			if( $this->client->updateClient($data, $ID) === FALSE) {
				$this->db->trans_rollback();
			} else {
				redirect(base_url() . 'client/viewClient/' . $ID);
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Cliente', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('client/editClient', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Editar Cliente'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'URL_POST'					=> base_url() .'client/editClient/' . $p->id_client
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" required')
			,'INPUT_DES_RUC'			=> form_input('input_des_ruc', $p->des_ruc, 'class="form-control" required')
			,'INPUT_DES_ADDRESS'		=> form_input('input_des_address', $p->des_address, 'class="form-control" required')
			,'INPUT_DES_PHONE'			=> form_input('input_des_phone', $p->des_phone, 'class="form-control" required')
			,'INPUT_ID_PERM'			=> form_hidden('input_id_client', $p->id_client)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Actualizar', 'btn-blue-alt', 'icon-save')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'client', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function viewClient() {
		acl('ind_show', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'client');
			exit();
		}
		$p = $this->client->getClientRow(['id_client' => $ID]);
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Cliente', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('client/viewClient', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Ver Cliente'
			,'BODY_SUBTITLE'			=> '(' . $p->des_name . ')'
			,'BODY_MENU'				=> ''
			,'INPUT_DES_NAME'			=> form_input('input_des_name', $p->des_name, 'class="form-control" readonly')
			,'INPUT_DES_RUC'			=> form_input('input_des_ruc', $p->des_ruc, 'class="form-control" readonly')
			,'INPUT_DES_ADDRESS'		=> form_input('input_des_address', $p->des_address, 'class="form-control" readonly')
			,'INPUT_DES_PHONE'			=> form_input('input_des_phone', $p->des_phone, 'class="form-control" readonly')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'client', 'btn-danger', 'icon-ban', 'Volver', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteClient() {
		acl('ind_del', null, 403);
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000) {
			header('Location: ' . base_url() . 'client');
			exit();
		}
		$p = $this->client->getClientRow(['id_client' => $ID]);
		if($this->input->post()) {
			if( $this->input->post('input_id_client', TRUE) == $ID) {
				if ($this->client->deleteClient($this->input->post('input_id_client', TRUE)) === FALSE) {
					$this->db->trans_rollback();
				} else {
					redirect(base_url() . 'client');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Cliente ' . $p->des_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Cliente - ' . $p->des_name
			,'BODY_SUBTITLE'			=> 'No se puede deshacer la eliminaci&oacute;n'
			,'BODY_MENU'				=> '(' . $p->id_client . ') ' . $p->des_name
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del Cliente "' . $p->des_name . '"'
			,'URL_POST'					=> base_url() . 'client/deleteClient/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_id_client', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-blue-alt', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'client', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function getClients() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('start');
			$limit = $this->input->post('length');
			$draw = $this->input->post('draw');
			if($this->input->post('search')['value'] != '') {
				$search = $this->input->post('search')['value'];
				$like = ['des_name' => $search];
				$result = $this->client->getClients(null, $like);
				$total = $this->client->getTotalClient(null, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['des_name' => $search];
					$result = $this->client->getClients(null, $like, $start, $limit);
					$total = $this->client->getTotalClient(null, $like);
				} else {
					$result = $this->client->getClients(null, null, $start, $limit);
					$total = $this->client->getTotalClient();
				}
			}
		} else {
			$result = $this->client->getClients();
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						array_push($records, [
							'id_client'			=> $r->id_client
							,'des_name'			=> $r->des_name
							,'des_ruc'			=> $r->des_ruc
							,'des_address'		=> $r->des_address
							,'des_phone'		=> $r->des_phone
						]);
					}
					$data = ['draw' => $draw
						,'recordsTotal' => $total
						,'recordsFiltered' => $total
						,'data' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			case 'json-ui':
				$rows = [];
				foreach($result as $k) {
					array_push($rows, [
							'id'		=> $k->id_client
							,'name'		=> $k->des_name
							,'ruc'		=> $k->des_ruc
							,'address'	=> $k->des_address
							,'phone'	=> $k->des_phone
					]);
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $rows ));
			break;
			default:
				
			break;
		}
	}
}
