<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class clientModel extends MX_Controller {
	
	const CLIENT = 'cc_client';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getClients($where = null, $like = null, $limit = null, $start = null) {
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get(self::CLIENT)->result();
    }
    
    function getClientRow($where) {
    	return $this->db->get_where(self::CLIENT, $where)->row();
    }
	
	function saveClient($data) {
		$this->db->insert(self::CLIENT, $data);
		return $this->db->insert_id();
	}
	
	function updateClient($data, $ID) {
		$this->db->trans_start();
		$this->db->update(self::CLIENT, $data, ['id_client' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteClient($ID) {
		$this->db->trans_start();
		$this->db->delete(self::CLIENT, ['id_client' => $ID]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalClient($where = null, $like = null, $start = null, $limit = null) {
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from(self::CLIENT);
		return $this->db->count_all_results();
	}
}
