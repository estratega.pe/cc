<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable-responsive.js"></script>

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">

<script type="text/javascript">

    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable( {
            responsive: true
            ,stateSave: false
            ,language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning�n dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "�ltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    } );

    $(document).ready(function() {
        $('.dataTables_filter input').attr("placeholder", "Buscar...");
    });

</script>

<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel-B">
	<div class="panel-B-body">
		<h3 class="title-hero">
		{BODY_DESCRIPTION}
		</h3>
		<div class="example-box-wrapper">
			<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
			<thead>
				<tr>
				{TH_TABLE}
					<th>{LABEL}</th>
				{/TH_TABLE}
				</tr>
			</thead>
			
			<tfoot>
				<tr>
				{TH_TABLE}
				    <th>{LABEL}</th>
				{/TH_TABLE}
				</tr>
			</tfoot>
			
			<tbody>
				{TR_TABLE}
				<tr>
					{TD_TABLE}
					<td>{TD_CONTENT}</td>
					{/TD_TABLE}
				</tr>
				{/TR_TABLE}
			</tbody>
			</table>
		</div>
	</div>
</div>