<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable-responsive.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">

<script type="text/javascript">
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			var grupo = $('#select_grupo option:selected').text();
			var select_grupo = data[{FILTER_NUMBER}] || 0;
			if ( ( $('#select_grupo').val() == 'X' ) || ( ( $('#select_grupo').val() != 'X' )  && ( grupo == select_grupo ) ) ) {
				return true;
			}
			return false;
		}
	);

    /* Datatables responsive */

    $(document).ready(function() {
        table = $('#datatable-responsive').DataTable( {
            responsive: true
            ,stateSave: true
            ,language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning�n dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "�ltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
		
		$('.dataTables_filter input').attr("placeholder", "Buscar...");
     
		// Event listener to the two range filtering inputs to redraw on input
		$('#select_grupo').on('change', function() {
			$('#datatable-responsive').DataTable().draw();
		} );
    } );
	

</script>

<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel-B">
	<div class="panel-B-body">
	<h3 class="title-hero">
	{BODY_DESCRIPTION}
	</h3>
		<div class="example-box-wrapper">
			<div class="row">
				<div class="col-md-6 form-horizontal bordered-row">
					<div class="form-group">
					</div>
				</div>
				<div class="col-md-6 form-horizontal bordered-row">
					<div class="form-group">
						<label class="col-sm-3 control-label">GRUPO</label>
						<div class="col-sm-6">
							{SELECT_GRUPO}
						</div>
					</div><p></p>
				</div>
			</div>
			<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
				<thead>
					<tr>
					{TH_TABLE}
						<th>{LABEL}</th>
					{/TH_TABLE}
					</tr>
				</thead>

				<tfoot>
					<tr>
					{TH_TABLE}
						<th>{LABEL}</th>
					{/TH_TABLE}
					</tr>
				</tfoot>

				<tbody>
					{TR_TABLE}
					<tr>
						{TD_TABLE}
						<td>{TD_CONTENT}</td>
						{/TD_TABLE}
					</tr>
					{/TR_TABLE}
				</tbody>
			</table>
		</div>
	</div>
</div>