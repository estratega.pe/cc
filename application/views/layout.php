<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html> 
<html lang="en">
<head>
{CONTENT_HEADER}
<!-- HELPERS -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/animate.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/backgrounds.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/boilerplate.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/border-radius.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/grid.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/page-transitions.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/spacing.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/typography.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/utils.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/colors.css">
 
<!-- ELEMENTS -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/badges.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/buttons.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/content-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/dashboard-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/forms.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/images.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/info-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/loading-indicators.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/menus.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/panel-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/response-messages.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/responsive-tables.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/ribbon.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/social-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/tables.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/tile-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/timeline.css">
 
<!-- ICONS -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/icons/fontawesome/fontawesome.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/icons/linecons/linecons.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/icons/spinnericon/spinnericon.css">
 
 
<!-- WIDGETS -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/accordion-ui/accordion.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/calendar/calendar.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/carousel/carousel.css">
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/justgage/justgage.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/morris/morris.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/piegage/piegage.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/xcharts/xcharts.css">
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/chosen/chosen.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/colorpicker/colorpicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/datatable/datatable.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/datepicker/datepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/datepicker-ui/datepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/daterangepicker/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/dialog/dialog.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/dropdown/dropdown.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/file-input/fileinput.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/input-switch/inputswitch.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/input-switch/inputswitch-alt.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/ionrangeslider/ionrangeslider.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jcrop/jcrop.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jgrowl-notifications/jgrowl.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/loading-bar/loadingbar.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/maps/vector-maps/vectormaps.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/markdown/markdown.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/modal/modal.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/multi-select/multiselect.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/multi-upload/fileupload.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/nestable/nestable.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/noty-notifications/noty.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/popover/popover.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/pretty-photo/prettyphoto.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/progressbar/progressbar.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/range-slider/rangeslider.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/slider-ui/slider.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/tabs-ui/tabs.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/theme-switcher/themeswitcher.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/timepicker/timepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/tooltip/tooltip.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/touchspin/touchspin.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/uniform/uniform.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/wizard/wizard.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/xeditable/xeditable.css">
 
<!-- SNIPPETS -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/chat.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/files-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/login-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/notification-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/progress-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/todo.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/user-profile.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/mobile-navigation.css">
 
<!-- APPLICATIONS -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/applications/mailbox.css">
 
<!-- Admin theme -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/admin/layout.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/admin/color-schemes/default.css">
 
<!-- Components theme -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/components/default.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/components/border-radius.css">
 
<!-- Admin responsive -->
 
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/responsive-elements.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/admin-responsive.css">
 
<!-- JS Core -->
 
<script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-core.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-core.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-widget.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-mouse.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-position.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/transition.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/modernizr.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-cookie.js"></script>

<script type="text/javascript" src="{BASE_URL}assets/widgets/sipjs/sip-0.7.5.min.js"></script>

<script type="text/javascript">
var BASE_URL = '{BASE_URL}assets/widgets/';
$(window).load(function(){
        setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
        
    });
</script>

{SOFTPHONE_SCRIPT}

</head>
    <body>
    <div id="sb-site">
		<div id="loading">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
	{CONTENT_HEADER_TOP}
	{CONTENT_ASIDE_MENU}
	<div id="page-content-wrapper">
    <div id="page-content">
	{CONTENT_BODY}
    </div>
</div>
</div>
<!-- Bootstrap Dropdown -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/dropdown/dropdown.js"></script>
 
<!-- Bootstrap Tooltip -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/tooltip/tooltip.js"></script>
 
<!-- Bootstrap Popover -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/popover/popover.js"></script>
 
<!-- Bootstrap Progress Bar -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/progressbar/progressbar.js"></script>
 
<!-- Bootstrap Buttons -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/button/button.js"></script>
 
<!-- Bootstrap Collapse -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/collapse/collapse.js"></script>
 
<!-- Superclick -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/superclick/superclick.js"></script>
 
<!-- Input switch alternate -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/input-switch/inputswitch-alt.js"></script>
 
<!-- Slim scroll -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/slimscroll/slimscroll.js"></script>
 
<!-- Content box -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/content-box/contentbox.js"></script>
 
<!-- Overlay -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/overlay/overlay.js"></script>
 
<!-- Widgets init for demo -->
 
<script type="text/javascript" src="{BASE_URL}assets/js-init/widgets-init.js"></script>
 
<!-- Theme layout -->
 
<script type="text/javascript" src="{BASE_URL}assets/themes/admin/layout.js"></script>
 
<!-- Theme switcher -->
 
<script type="text/javascript" src="{BASE_URL}assets/widgets/theme-switcher/themeswitcher.js"></script>
</body>
</html>