<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="page-wrapper">
	<div id="page-header" class="bg-gradient-9">
		<div id="mobile-navigation">
			<button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar" aria-expanded="false" aria-controls="navbar">
				<span></span>
			</button>
			<a href="{BASE_URL}" class="logo-content-small" title="{SITE_NAME}"></a>
		</div>
		<div id="header-logo" class="logo-bg">
			<a href="{BASE_URL}" class="logo-content-big" title="{SITE_NAME}">
				Monarch <i>UI</i>
				<span>The perfect solution for user interfaces</span>
			</a>
			<a href="{BASE_URL}" class="logo-content-small" title="{SITE_NAME}">
				Monarch <i>UI</i>
				<span>The perfect solution for user interfaces</span>
			</a>
			<a id="close-sidebar" href="#" title="Close sidebar">
				<i class="glyph-icon icon-angle-left"></i>
			</a>
		</div>
		<div id="header-nav-left">
			<div class="user-account-btn dropdown">
				<a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
					<img width="28" src="{BASE_URL}assets/image-resources/gravatar.jpg" alt="Profile image">
					<span>{USER_NAME}</span>
					<i class="glyph-icon icon-angle-down"></i>
				</a>
				<div class="dropdown-menu float-left">
					<div class="box-sm">
						<div class="login-box clearfix">
							<div class="user-img">
								<!--<a href="#" title="" class="change-img">Change photo</a>-->
								<img src="{BASE_URL}assets/image-resources/gravatar.jpg" alt="">
							</div>
						<div class="user-info">
							<span>
							{USER_NAME}
							<!--<i>UX/UI developer</i>-->
							</span>
							<a href="#" title="Edit profile">Editar perfil</a>
						</div>
						</div>
						<div class="pad5A button-pane button-pane-alt text-center">
							<a href="{BASE_URL}login/loginOut" class="btn display-block font-normal btn-danger">
								<i class="glyph-icon icon-power-off"></i>Desconectar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div><!-- #header-nav-left -->
		<div id="header-nav-right">
			<a href="#" class="popover-button" title="" data-placement="bottom" data-id="#popover-search" data-original-title="Search">
			<i class="glyph-icon icon-search"></i>
			</a>
			<div class="hide" id="popover-search">
				<div class="pad5A box-md">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search terms here ...">
						<span class="input-group-btn">
							<a class="btn btn-primary" href="#">Search</a>
						</span>
					</div>
				</div>
			</div>

			<a class="header-btn" id="logout-btn" href="{BASE_URL}login/loginOut" title="Desconectar">
				<i class="glyph-icon icon-linecons-lock"></i>
			</a>

		</div><!-- #header-nav-right -->
	</div>



</div>