<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div id="page-sidebar">
	<div class="scroll-sidebar">
		<ul id="sidebar-menu" class="sf-js-enabled sf-arrows">
			<li class="header"><span>Overview</span></li>
			<li>
			<a href="{BASE_URL}" title="Admin Dashboard" class="sfActive">
				<i class="glyph-icon icon-linecons-tv"></i>
				<span>Inicio</span>
			</a>
			</li>
			<li class="divider"></li>
			<li class="header"><span>Registros</span></li>
			<li class="no-menu">
				<a href="{BASE_URL}campaign" title="Campa&ntilde;as">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Campa&ntilde;as</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}contact" title="Contactos">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Contactos</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}agent" title="Agentes">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Agentes</span>
				</a>
			</li>
			<!-- START ADMINISTRACION -->
			<li class="divider"></li>
			<li class="header"><span>Administraci&oacute;n</span></li>
			<li class="no-menu">
				<a href="{BASE_URL}perm" title="Permisos">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Permisos</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}group" title="Tipos de Usuarios">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Tipos de Usuario</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}queue" title="Colas">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Colas</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}user" title="Usuarios">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Usuarios</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}turn" title="Turnos">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Turnos</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}pause" title="Pausas">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Pausas</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}client" title="Clientes">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Clientes</span>
				</a>
			</li>
			<li class="no-menu">
				<a href="{BASE_URL}type_campaign" title="Tipos de Campa&ntilde;as">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Tipos de Campa&ntilde;as</span>
				</a>
			</li>

			<li class="divider"></li>
			<li class="header"><span>Reportes</span></li>
			<li class="no-menu">
				<a href="#" title="Resultados de evaluaci&oacute;n">
					<i class="glyph-icon icon-linecons-beaker"></i>
					<span>Resultados de evaluaci&oacute;n</span>
				</a>
			</li>
		</ul><!-- #sidebar-menu -->
	</div>
</div>