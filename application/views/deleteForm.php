<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/easyui/themes/bootstrap/lgonzales.css">
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel-B">
	<div class="panel-B-body">
		<h3 class="title-hero">
		{BODY_DESCRIPTION}
		</h3>
		<div class="example-box-wrapper">
			<form method="post" action="{URL_POST}" class="">
				<div class="row">
                    <div class="col-md-6 form-horizontal">
						<div class="form-group">
							<div class="col-sm-3">
							{INPUT_DELETE_ID}
							</div>
                            <div class="col-sm-9">
                                {BUTTON_SUBMIT}
                            </div>
						</div><p></p>
					</div>
					<div class="col-md-6 form-horizontal">
						<div class="form-group">
							<div class="col-sm-6">
							
							</div>
							<div class="col-sm-6">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>