<?php
defined('BASEPATH') OR exit('No direct script access allowed');

#Construye la pagina
if ( ! function_exists('renderPage')) {
	function renderPage($data = 'X') {
		if($data == 'X')
			return false;
			$CI = & get_instance();
			$menu = $CI->parser->parse('menu', ['BASE_URL' => base_url()], TRUE);
			$CI->parser->parse('layout', ['BASE_URL' => base_url(),
					'CONTENT_HEADER' => $CI->parser->parse('header', $data['HEADER'], TRUE)
					,'CONTENT_HEADER_TOP'	=> $CI->parser->parse('header_top', ['BASE_URL' => base_url(), 'USER_NAME' => $CI->session->userdata('USER')->des_user], TRUE)
					,'CONTENT_ASIDE_MENU'	=> $CI->parser->parse('menu', ['BASE_URL' => base_url()], TRUE)
					//,'CONTENT_MENU'			=> $menu
					,'CONTENT_BODY'			=> $data['BODY']
					,'SOFTPHONE_SCRIPT'		=> generateSoftPhone()
			]);
	}
}

#ACL
if ( ! function_exists('acl')) {
	function acl ($lvl = null, $module = null, $page = null) {
		$CI = &get_instance();
		if( !is_null($lvl) ) {
			if($CI->session->userdata('USER')->id_type_user == 0x0001) {
				$retorno = 'Y';
			} else {
				$module = (is_null($module)) ? $CI->uri->segment(1) : $module;
				$module = filter_var(strtolower($module), FILTER_SANITIZE_STRING);
				$perm = $CI->session->userdata('PERMS');
				$modules = array_keys($perm);
				if(in_array($module, $modules)) {
					if(in_array($lvl,$perm[$module]))
						$retorno = 'Y';
					else
						$retorno = 'N';
				} else {
					$retorno = 'N';
				}
			}
		} else {
			$retorno = 'N';
		}
		if($retorno == 'Y') {
			return TRUE;
		} else {
			if(is_null($page))
				return FALSE;
			else
				show_error('Acceso restringido', $page);
		}

	}
}

#Rellena de ceros
if ( ! function_exists('zerofill')) {
	function zerofill ($num, $zerofill = 4)
	{
		return str_pad($num, $zerofill, '0', STR_PAD_LEFT);
	}
}

#Crea link
if ( ! function_exists('createLink')) {
	function createLink($target, $color, $icon, $text, $with = null) {
		if(is_null($with))
			$return =  '<a href="' . $target . '" title="' . $text . '"><span class="btn ' . $color . '"><i class="glyph-icon ' . $icon . '"></i></span></a>';
		else
			$return  = '<a href="' . $target . '"><span class="btn ' . $color . '"><i class="glyph-icon ' . $icon . '"></i>&nbsp;' . $text . '</span></a>';
		return $return;
	}
}

#Crea Button Submit
if ( ! function_exists('createSubmitButton')) {
	function createSubmitButton($text, $color, $_icon = 'X') {
		$icon = '';
		if($_icon != 'X')
			$icon = '<i class="glyph-icon ' . $_icon . '"></i>&nbsp';
		return '<button class="btn ' . $color . '">' . $icon . $text . '</button>';
	}
}

#Formatea la fecha para guardar en DB
if ( ! function_exists('converDate')) {
	function converDate ($fecha, $limitador = '/')
	{
		$f = explode($limitador, $fecha);
		return $f[2] . '-' . $f[0] . '-' . $f[1];
	}
}

#Formatea la feha para mostrarla al usuario
if ( ! function_exists('revertDate')) {
	function revertDate ($fecha, $limitador = '-')
	{
		$f = explode($limitador, $fecha);
		return $f[1] . '/' . $f[2] . '/' . $f[0];
	}
}

#Regenera el archivo de agentes
if (! function_exists('regenerateAgents')) {
	function regenerateAgents() {
		$CI = &get_instance();
		$agents = $CI->db->get('gen_agent')->result();
		$string = "";
		foreach($agents as $k) {
			$version = $CI->config->item('AST_VERSION');
			switch($version) {
				case 2;
					$string .= "[$k->des_machine_name]\n";
					$string .= "fullname=$k->des_name\n\n";
				break;
				default:
					$string .= "agent => $k->des_machine_name,$k->des_machine_name,$k->des_name\n";
				break;
			}
		}
		file_put_contents("/etc/asterisk/portal_agents.conf", $string, LOCK_EX);
	}
}

#Regenera el archivo de colas
if (! function_exists('regenerateQueues')) {
	function regenerateQueues() {
		$CI = &get_instance();
		$campaign = $CI->db->get_where('cc_campaign', ['ind_status' => 1])->result();
		foreach($campaign as $c) {
			$queues = $CI->db->get_where('gen_queue_campaign', ['id_campaign' => $c->id_campaign])->result();
			$agents = agentFromCampaign($c->id_campaign);
			foreach($queues as $k) {
				$q = $CI->db->get_where('gen_queue', ['id_queue' => $k->id_queue])->row();
				$string .= "[$q->des_machine_name]\n";
				$string .= $agents;
				$string .= "\n";
			}
		}
		file_put_contents("/etc/asterisk/portal_queues.conf", $string, LOCK_EX);
	}
}

#Devuelve los agentes de una campa�a
if (! function_exists('agentFromCampaign')) {
	function agentFromCampaign($campaign = FALSE) {
		if($campaign === FALSE)
			return FALSE;
		$string = "";
		$CI = &get_instance();
		$agents = $CI->db->get_where('cc_campaign_agent', ['id_campaign' => $campaign])->result();
		foreach($agents as $a) {
			$k = $CI->db->get_where('gen_agent', ['id_agent' => $a->id_agent])->row();
			$string .= "member => Local/$k->des_machine_name@agents,0,$k->des_name,Agent:$k->des_machine_name\n";
		}
		return $string;
	}
}

#Regenera el Archivo portal_sip.conf
if (! function_exists('regenerateSip')) {
	function regenerateSip() {
		$string = "";
		$CI = &get_instance();
		$uAgents = $CI->db->get('gen_user_agent')->result();
		foreach($uAgents as $k) {
			$a = $CI->db->get_where('gen_agent', ['id_agent' => $k->id_agent])->row();
			$u = $CI->db->get_where('gen_user', ['id_user' => $k->id_user])->row();
			$string .= "[$a->des_machine_name]\n";
			$string .= "type=friend\n";
			$string .= "defaultUser=$a->des_machine_name\n";
			$string .= "username=$a->des_machine_name\n";
			$string .= "context=contactcenter\n";
			$string .= "secret=$u->des_passwd\n";
			$string .= "host=dynamic\n";
			$string .= "\n";
		}
		file_put_contents("/etc/asterisk/portal_sip.conf", $string, LOCK_EX);
	}
}

#Generate SoftPhone
if ( ! function_exists('generateSoftPhone')) {
	function generateSoftPhone() {
		$CI = &get_instance();
		$r = $CI->db->get_where('gen_user_agent', ['id_user' => $CI->session->userdata('USER')->id_user])->row();
		if(!is_object($r)) {
			return FALSE;
		} else {
			$agent = $CI->db->get_where('gen_agent', ['id_agent' => $r->id_agent])->row();
			$string = '<script type="text/javascript" src="' . base_url() . 'assets/widgets/webphone/webphone_api.js?wp_webphonebasedir=/cc/assets/widgets/webphone/"></script>';
			$string .= "\n<script>
			webphone_api.onLoaded(function () {
				webphone_api.setparameter('webphonebasedir', '".base_url()."assets/widgets/webphone/');
				webphone_api.setparameter('serveraddress', '".$CI->config->item('AST_SERVER').":5060');
				webphone_api.setparameter('username', '".$agent->des_machine_name."');
				webphone_api.setparameter('password', '".$CI->session->userdata('USER')->des_passwd."');
				webphone_api.start();
			});
			</script>";
			//webphone_api.setparameter('webphonebasedir', '".base_url()."assets/widgets/webphone/');
			//return $string;
		}
	};
	
};
/*
if ( ! function_exists('generateSoftPhone_SIP')) {
	function generateSoftPhone_SIP() {
		$CI = &get_instance();
		$r = $CI->db->get_where('gen_user_agent', ['id_user' => $CI->session->userdata('USER')->id_user])->row();
		if(!is_object($r)) {
			return FALSE;
		} else {
			$agent = $CI->db->get_where('gen_agent', ['id_agent' => $r->id_agent])->row();
			$string = "
var config = {
  uri: '".$agent->des_machine_name."@".$CI->config->item('AST_SERVER')."',
  wsServers: 'ws://".$CI->config->item('AST_SERVER').":8088/ws',
  authorizationUser: '".$agent->des_machine_name."',
  password: '".$CI->session->userdata('USER')->des_passwd."',
  hackIpInContact: true,
  level: 0,
};

var ua = new SIP.UA(config);
					
ua.on('invite', function (session) {
  session.accept();
});

";
			return $string;
		}
		
	}
}
*/
	
/*
 #Regenera el Archivo portal_sip.conf
 if (! function_exists('regenerateSip')) {
 function regenerateSip() {
 $string = "";
 $CI = &get_instance();
 $uAgents = $CI->db->get('gen_user_agent')->result();
 foreach($uAgents as $k) {
 $a = $CI->db->get_where('gen_agent', ['id_agent' => $k->id_agent])->row();
 $u = $CI->db->get_where('gen_user', ['id_user' => $k->id_user])->row();
 $string .= "[$a->des_machine_name]\n";
 $string .= "type=friend\n";
 $string .= "defaultUser=$a->des_machine_name\n";
 $string .= "username=$a->des_machine_name\n";
 $string .= "context=contactcenter\n";
 $string .= "secret=$u->des_passwd\n";
 $string .= "host=dynamic\n";
 $string .= "force_avp=yes\n";
 $string .= "avpf=yes\n";
 $string .= "icesupport=yes\n";
 $string .= "encryption=yes\n";
 $string .= "directmedia=no\n";
 //$string .= "dtlsrekey=60\n";
 //$string .= "dtlsverify=no\n";
 //$string .= "dtlscertfile=/etc/asterisk/keys/asterisk.crt\n";
 //$string .= "dtlsprivatekey=/etc/asterisk/keys/asterisk.key\n";
 //$string .= "dtlssetup=actpass\n";
$string .= "\n";
}
file_put_contents("/etc/asterisk/portal_sip.conf", $string, LOCK_EX);
}
}
*/