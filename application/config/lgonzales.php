<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Asterisk Version
 * Asterisk 1.8 => 1
 * Asterisk 11	=> 1
 * Asterisk 13	=> 2
 */
$config['AST_VERSION'] = 2;

/**
 * Asterisk Server
 */
$config['AST_SERVER'] = '45.55.198.251';
/**
 * Tipos Sexo
 */
$config['TIPOS_SEXO'] = ['M' => 'MASCULINO', 'F' => 'FEMENINO'];

/**
 * Opciones SI/NO
 */
$config['OPTIONS_SINO'] = [0 => 'NO', 1 => 'SI'];

/**
 * CONFIG UPLOAD
 **/
$config['FOLDER_UPLOAD'] = APPPATH . '../assets/uploads/';

/**
 * CONFIG IMAGE UPLOAD
 */
$config['IMAGE_ALLOW'] = 'jpg|jpeg|png|pneg';
$config['IMAGE_MAX_SIZE'] = 5120;
$config['IMAGE_MAX_WIDTH'] = 1500;
$config['IMAGE_MAX_HEIGHT'] = 1500;
$config['IMAGE_ENCRYPT_NAME'] = TRUE;
