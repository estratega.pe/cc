CREATE TABLE IF NOT EXISTS `queue_log` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`time` varchar(40) default NULL,
	`callid` varchar(32) NOT NULL default '',
	`queuename` varchar(32) NOT NULL default '',
	`agent` varchar(32) NOT NULL default '',
	`event` varchar(32) NOT NULL default '',
	`data` varchar(255) NOT NULL default '',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `agent_status` (
	`agentId` varchar(40) NOT NULL default '',
	`agentName` varchar(40) default NULL,
	`agentStatus` varchar(30) default NULL,
	`timestamp` timestamp NULL default NULL,
	`callid` double(18,6) unsigned default '0.000000',
	`queue` varchar(20) default NULL,
PRIMARY KEY (`agentId`),
KEY `agentName` (`agentName`),
KEY `agentStatus` (`agentStatus`,`timestamp`,`callid`),
KEY `queue` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `call_status` (
	`callId` double(18,6) NOT NULL,
	`callerId` varchar(13) NOT NULL,
	`status` varchar(30) NOT NULL,
	`timestamp` timestamp NULL default NULL,
	`queue` varchar(25) NOT NULL,
	`position` varchar(11) NOT NULL,
	`originalPosition` varchar(11) NOT NULL,
	`holdtime` varchar(11) NOT NULL,
	`keyPressed` varchar(11) NOT NULL,
	`callduration` int(11) NOT NULL,
PRIMARY KEY (`callId`),
KEY `callerId` (`callerId`),
KEY `status` (`status`),
KEY `timestamp` (`timestamp`),
KEY `queue` (`queue`),
KEY `position` (`position`,`originalPosition`,`holdtime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;