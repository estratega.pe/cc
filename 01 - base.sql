INSERT INTO `gen_type_user` (`id_type_user`, `des_name`, `des_machine_name`) VALUES
(1, 'Administrador', 'admin'),
(2, 'Supervisor', 'supervisor'),
(3, 'Agente', 'agent');

INSERT INTO `gen_user` (`id_user`, `des_user`, `des_firstname`, `des_lastname`, `des_email`, `des_passwd`, `des_salt`, `ind_status`, `id_type_user`) VALUES
(1, 'lgonzales', 'Jose Luis', 'Huamani Gonzales', 'Aisenhaim@gmail.com', '123456', '12345', b'1', 1);

INSERT INTO `gen_perm` (`id_perm`, `des_name`, `des_machine_name`) VALUES
(1, 'Permisos', 'perm'),
(2, 'Tipos de Usuario', 'group'),
(3, 'Usuarios', 'user'),
(4, 'Colas', 'queue'),
(5, 'Agentes', 'agent'),
(6, 'Turnos', 'turn'),
(7, 'Campañas', 'campaign'),
(8, 'Asignar agentes a Campañas', 'agent_to_campaign');